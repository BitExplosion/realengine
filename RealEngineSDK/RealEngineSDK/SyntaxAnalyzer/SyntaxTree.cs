﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.WhenStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class SyntaxTree
    {
        private List<string> lineOfCode;
        private readonly List<SymbolTableIterator> syntaxParsers;

        public SyntaxTree(List<string> lineOfCode, SyntaxAnalyzerI blockCodeEvaluator)
        {
            this.lineOfCode = lineOfCode;
            syntaxParsers = new List<SymbolTableIterator>();
            syntaxParsers.Add(new CreateVariableSyntaxParser());
            syntaxParsers.Add(new AssignmentSyntaxParser(new ArithmeticSyntaxParser()));
            syntaxParsers.Add(new IfStatementSyntaxParser(blockCodeEvaluator, new ArithmeticSyntaxParser()));
            syntaxParsers.Add(new WhenStatementSyntaxParser(blockCodeEvaluator, new ArithmeticSyntaxParser()));
        }


        private object parseUsingFirstAvailableSyntaxAnalyzer(List<string> lineOfCode)
        {
            try
            {
                return syntaxParsers[0].evaluateLineOfCode(lineOfCode);
            }
            catch (SyntaxAnalyzerException e)
            {
                syntaxParsers.RemoveAt(0);
                if (syntaxParsers.Count == 0)
                {
                    throw e;
                }
            }
            return null;
        }


        public object evaluateLineUsingTree(List<string> lineOfCode)
        {
            try
            {
                while (syntaxParsers.Count != 0)
                {
                    var syntaxItem = parseUsingFirstAvailableSyntaxAnalyzer(lineOfCode);
                    if (syntaxItem != null)
                    {
                        return syntaxItem;
                    }
                }
                return null;
            }
            catch (SyntaxAnalyzerException e)
            {
                throw e;
            }
        }
    }
}