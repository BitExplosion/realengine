﻿using System.Collections.Generic;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class SyntaxAnalyzer : SyntaxAnalyzerI
    {
        public List<object> analyzeSoureCode(List<string> tokens)
        {
            if (tokens.Count == 0)
            {
                return new List<object>();
            }
            var operations = new List<object>();
            operations.Add(syntaxAnalysisPerLine(tokens));
            return operations;
        }


        public object syntaxAnalysisPerLine(List<string> tokenizedLinesOfCode)
        {
            return new SyntaxTree(tokenizedLinesOfCode, this).evaluateLineUsingTree(tokenizedLinesOfCode);
        }
    }
}