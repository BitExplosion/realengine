﻿using System.Collections.Generic;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public interface SyntaxAnalyzerI
    {
        List<object> analyzeSoureCode(List<string> tokens);
    }
}