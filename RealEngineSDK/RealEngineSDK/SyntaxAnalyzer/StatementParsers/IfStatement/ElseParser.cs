﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.IfStatement
{
    internal class ElseParser : SymbolParser
    {
        private readonly IfStatementOperation ifStatement;

        public ElseParser(IfStatementOperation ifStatement)
        {
            this.ifStatement = ifStatement;
        }

        public void onEvaluateSymbol(string token)
        {
            if (token != "else")
            {
                throw new SyntaxAnalyzerException("IF STATEMENT MUST CONTAIN VALID ELSE OR IFELSE CLAUSE");
            }
            ifStatement.ElseCondition.Condition.Symbols.Add("");
            ifStatement.ElseCondition.Condition.Symbols.Add("");
            ifStatement.ElseCondition.Condition.Symbols.Add("==");
        }
    }
}