﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.IfStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.Utility;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement
{
    public class IfStatementSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        private readonly SymbolTableIterator booleanSyntaxParser;
        private readonly IfStatementOperation ifStatementOperation;
        private readonly SyntaxAnalyzerI syntaxAnalyzer;

        public IfStatementSyntaxParser(SyntaxAnalyzerI syntaxAnalyzer, SymbolTableIterator booleanSyntaxParser)
            : base(new IfStatementOperation())
        {
            ifStatementOperation = (IfStatementOperation) operation;
            addParsers(new KeywordParser("if"));
            addParsers(new KeywordParser("then"));
            addParsers(new KeywordParser("end"));
            this.syntaxAnalyzer = syntaxAnalyzer;
            this.booleanSyntaxParser = booleanSyntaxParser;
        }

        private void readyElseParsing(IfStatementOperation operation)
        {
            operation.ElseCondition = new IfStatementOperation();
            addParsers(new ElseParser(operation));
            addParsers(new KeywordParser("then"));
            addParsers(new KeywordParser("end"));
        }

        private void readyElseIfParsing(IfStatementOperation operation)
        {
            operation.ElseIfConditions.Add(new IfStatementOperation());
            addParsers(new KeywordParser("elseif"));
            addParsers(new KeywordParser("then"));
            addParsers(new KeywordParser("end"));
        }

        protected override List<string> preIterateModifier(List<string> ifStatementTokens)
        {
            var queryEngine = new TokenQueryEngine(ifStatementTokens);
            var ifStatementReservedWordTokens = queryEngine.queryReservedWordTokens();
            foreach (var token in ifStatementReservedWordTokens)
            {
                if (token == "else")
                {
                    readyElseParsing(ifStatementOperation);
                }
                else if (token == "elseif")
                {
                    readyElseIfParsing(ifStatementOperation);
                }
            }

            var ifConditionTokens = queryEngine.queryNextIfBooleanStatementTokens();
            if (ifConditionTokens.Count == 0)
            {
                throw new SyntaxAnalyzerException("IF CLAUSE HAS NO BOOLEAN CONDITION");
            }
            ifStatementOperation.Condition =
                (ArithmeticOperation) booleanSyntaxParser.evaluateLineOfCode(ifConditionTokens);
            ifStatementOperation.blockOperations = syntaxAnalyzer.analyzeSoureCode(queryEngine.queryNextBlockOfCode());

            foreach (var elseIfOperation in ifStatementOperation.ElseIfConditions)
            {
                var ifStatementConditions = queryEngine.queryNextIfBooleanStatementTokens();
                if (ifStatementConditions.Count == 0)
                {
                    throw new SyntaxAnalyzerException("ELSE IF CLAUSE DOES NOT HAVE CONDITION");
                }
                booleanSyntaxParser.clearParsers();
                elseIfOperation.Condition =
                    (ArithmeticOperation) booleanSyntaxParser.evaluateLineOfCode(ifStatementConditions);
                elseIfOperation.BlockOperations = syntaxAnalyzer.analyzeSoureCode(queryEngine.queryNextBlockOfCode());
            }

            if (ifStatementOperation.ElseCondition != null)
            {
                ifStatementOperation.ElseCondition.BlockOperations =
                    syntaxAnalyzer.analyzeSoureCode(queryEngine.queryNextBlockOfCode());
            }
            return ifStatementReservedWordTokens;
        }
    }
}