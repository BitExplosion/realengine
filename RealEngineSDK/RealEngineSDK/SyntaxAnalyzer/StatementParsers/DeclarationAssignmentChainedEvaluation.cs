﻿using System.Collections.Generic;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class DeclarationAssignmentChainedSyntaxParser : SymbolSyntaxParser
    {
        public object evaluateLineOfCode(List<string> tokenizedLinesOfCode)
        {
            var createVariable = new CreateVariableSyntaxParser();
            var assignment = new AssignmentSyntaxParser(new ArithmeticSyntaxParser());
            var createOperation = (CreateVariableOperation) createVariable.evaluateLineOfCode(tokenizedLinesOfCode);
            var declarationAssignment = new DeclarationAssignmentChainedOperation();
            declarationAssignment.Create =
                (CreateVariableOperation) createVariable.evaluateLineOfCode(tokenizedLinesOfCode);
            tokenizedLinesOfCode = removeCreateVariableTokens(tokenizedLinesOfCode);
            declarationAssignment.Assignment = (AssignmentOperation) assignment.evaluateLineOfCode(tokenizedLinesOfCode);

            return declarationAssignment;
        }

        private bool tokenIsNotKeywordOrVariableType(string token)
        {
            return !TokenIdentifier.isVariableCreationKeywordValid(token) && !TokenIdentifier.isThisAVariabelType(token);
        }

        private void addTokenToNewListIfTokenIsValid(List<string> newTokenList, string token)
        {
            if (tokenIsNotKeywordOrVariableType(token))
            {
                newTokenList.Add(token);
            }
        }

        private List<string> removeCreateVariableTokens(List<string> tokens)
        {
            var newTokenList = new List<string>();
            foreach (var token in tokens)
            {
                addTokenToNewListIfTokenIsValid(newTokenList, token);
            }
            return newTokenList;
        }
    }
}