﻿using System;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General
{
    public class OperationValidator
    {
        private readonly object operation;

        public OperationValidator(object operation)
        {
            this.operation = operation;
        }

        public bool isOperationOfValidType(Type type)
        {
            return operation.GetType() == type && operation != null && type != null;
        }
    }
}