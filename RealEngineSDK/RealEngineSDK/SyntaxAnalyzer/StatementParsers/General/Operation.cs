﻿namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General
{
    public class Operation
    {
        public static int NONOPERATION = 0;
        private OperationDetails operationDetails;

        public Operation()
        {
            operationDetails = new OperationDetails(NONOPERATION);
        }
    }
}