﻿namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General
{
    public interface ReoccuringParser
    {
        bool stopPushBack(string token);
    }
}