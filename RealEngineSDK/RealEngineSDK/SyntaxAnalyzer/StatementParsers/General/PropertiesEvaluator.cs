﻿using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General
{
    public class PropertiesParser : ReoccuringParser
    {
        public bool stopPushBack(string token)
        {
            return TokenIdentifier.isVariableCreationKeywordValid(token);
        }

        public void onEvaluateSymbol(Properties operation, string token)
        {
            operation.addProperties(token);
        }
    }
}