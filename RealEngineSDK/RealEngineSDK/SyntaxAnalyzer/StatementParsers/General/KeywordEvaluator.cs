﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.General
{
    public class KeywordParser : SymbolParser
    {
        private readonly string keyword = "";

        public KeywordParser(string keyword)
        {
            this.keyword = keyword;
        }

        public void onEvaluateSymbol(string token)
        {
            if (token != keyword)
            {
                throw new SyntaxAnalyzerException(keyword + " MISSING FROM STATEMENT");
            }
        }
    }
}