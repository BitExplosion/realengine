﻿using System.Collections.Generic;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General
{
    public class Properties
    {
        public static int PROPERTIESKEY = 6;
        private OperationDetails operationDetails;

        protected List<string> properties;

        public Properties(int passedKey)
        {
            properties = new List<string>();
            operationDetails = new OperationDetails(PROPERTIESKEY);
        }

        public void addProperties(string property)
        {
            properties.Add(property);
        }
    }
}