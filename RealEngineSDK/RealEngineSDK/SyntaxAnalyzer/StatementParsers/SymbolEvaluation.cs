﻿using System.Collections.Generic;

namespace RealEngineSDK.SyntaxAnalyzer
{
    internal interface SymbolSyntaxParser
    {
        object evaluateLineOfCode(List<string> linesOfCode);
    }
}