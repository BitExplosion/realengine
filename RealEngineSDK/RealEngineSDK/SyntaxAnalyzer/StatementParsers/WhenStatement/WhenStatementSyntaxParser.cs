﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.Utility;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.WhenStatement
{
    public class WhenStatementSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        private readonly SymbolTableIterator booleanSyntaxParser;
        private readonly SyntaxAnalyzerI syntaxAnalyzer;
        private readonly WhenStatementOperation whenStatementOperation;

        public WhenStatementSyntaxParser(SyntaxAnalyzerI syntaxAnalyzer, SymbolTableIterator booleanSymbolSyntaxParser)
            : base(new WhenStatementOperation())
        {
            whenStatementOperation = (WhenStatementOperation) operation;
            addParsers(new KeywordParser("when"));
            addParsers(new KeywordParser("then"));
            addParsers(new KeywordParser("end"));
            this.syntaxAnalyzer = syntaxAnalyzer;
            booleanSyntaxParser = booleanSymbolSyntaxParser;
        }

        protected override List<string> preIterateModifier(List<string> whenStatementTokens)
        {
            var query = new TokenQueryEngine(whenStatementTokens);
            var booleanCondition = query.queryWhenCondition();
            if (booleanCondition.Count != 0)
            {
                whenStatementOperation.WhenCondition =
                    (ArithmeticOperation) booleanSyntaxParser.evaluateLineOfCode(query.queryWhenCondition());
            }
            else
            {
                throw new SyntaxAnalyzerException("NO BOOLEAN OPERATION FOUND");
            }
            whenStatementOperation.BlockOperations = syntaxAnalyzer.analyzeSoureCode(query.queryNextBlockOfCode());
            return query.queryReservedWhenWordTokens();
        }
    }
}