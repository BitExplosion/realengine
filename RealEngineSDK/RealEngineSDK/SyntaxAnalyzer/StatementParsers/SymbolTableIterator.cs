﻿using System;
using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public abstract class SymbolTableIterator
    {
        protected object operation;
        protected List<SymbolParser> Parsers;
        protected int symbolPosition;
        protected int totalNumberOfSymbols;

        public SymbolTableIterator(object operation)
        {
            checkIfObjectHasAttribute(operation);
            this.operation = operation;
            Parsers = new List<SymbolParser>();
        }

        private void checkIfObjectHasAttribute(object objectToCheck)
        {
            if (objectToCheck.GetType().GetCustomAttributes(typeof(OperationAttribute), true).Length <= 0)
            {
                throw new ArgumentException("OPERATION IS NOT OPERATION OBJECT");
            }
        }

        protected void iterateThroughSymbols(List<string> tokenizedLinesOfCode)
        {
            tokenizedLinesOfCode = preIterateModifier(tokenizedLinesOfCode);
            var token = 0;
            while (symbolPosition < totalNumberOfSymbols && token < tokenizedLinesOfCode.Count)
            {
                onIterate(tokenizedLinesOfCode[token]);
                token++;
            }
            if (symbolPosition < totalNumberOfSymbols)
            {
                throw new SyntaxAnalyzerException("COULD NOT EVALUATE EXPRESSION");
            }
            symbolPosition = 0;
        }

        public void clearParsers()
        {
            totalNumberOfSymbols = 0;
            Parsers.Clear();
            operation = Activator.CreateInstance(operation.GetType());
        }

        protected void addParsers(SymbolParser symbolSyntaxParser)
        {
            Parsers.Add(symbolSyntaxParser);
            totalNumberOfSymbols++;
        }


        protected void removeParser(SymbolParser SyntaxParser)
        {
            Parsers.Remove(SyntaxParser);
            totalNumberOfSymbols--;
        }

        protected void removeParserAtPosition(int position)
        {
            Parsers.RemoveAt(position);
            totalNumberOfSymbols--;
        }

        protected abstract List<string> preIterateModifier(List<string> tokens);

        protected bool containsParser(SymbolParser SyntaxParser)
        {
            return Parsers.Contains(SyntaxParser);
        }

        public object evaluateLineOfCode(List<string> tokenizedLinesOfCode)
        {
            iterateThroughSymbols(tokenizedLinesOfCode);
            return operation;
        }


        protected virtual void onIterate(string token)
        {
            Parsers[symbolPosition].onEvaluateSymbol(token);
            symbolPosition++;
        }
    }
}