﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class ArithmeticSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        public ArithmeticSyntaxParser() : base(new ArithmeticOperation())
        {
        }

        protected override List<string> preIterateModifier(List<string> tokens)
        {
            var postFixConverter = new InfixToPostFixConverter(convertNotToGreater(tokens));
            var postFixNotationTokens = postFixConverter.convert();
            foreach (var token in postFixNotationTokens)
            {
                addParserForArithmeticStatement(token);
            }

            if (postFixNotationTokens.Count%2 == 0)
            {
                throw new SyntaxAnalyzerException("INVALID NUMBER OF SYMBOLS");
            }
            return postFixNotationTokens;
        }

        private List<string> convertNotToGreater(List<string> tokens)
        {
            var convertedNotBooleanTokens = new List<string>();
            for (var tokenIndex = 0; tokenIndex < tokens.Count; tokenIndex++)
            {
                if (tokens[tokenIndex] == "!")
                {
                    if (tokenIndex + 1 < tokens.Count)
                    {
                        convertedNotBooleanTokens.Add(tokens[tokenIndex + 1]);
                        convertedNotBooleanTokens.Add(">");
                    }
                    else
                    {
                        throw new InvalidOperandException();
                    }
                }
                else
                {
                    convertedNotBooleanTokens.Add(tokens[tokenIndex]);
                }
            }
            return convertedNotBooleanTokens;
        }


        private void addParserForArithmeticStatement(string token)
        {
            if (!new OperandUtils().isTokenOperand(token))
            {
                addParsers(new EvaluateOperand((ArithmeticOperation) operation));
            }
            else
            {
                addParsers(new EvaluateValidOperationSymbol((ArithmeticOperation) operation));
            }
        }
    }
}