﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.Arithmetic
{
    public class InfixToPostFixConverter
    {
        private readonly List<string> infixNotationTokens = new List<string>();
        private readonly Stack<string> operandStack = new Stack<string>();
        private readonly Dictionary<string, int> orderPrecedence = new Dictionary<string, int>();
        private readonly List<string> postFixNotationTokens = new List<string>();

        public InfixToPostFixConverter(List<string> infixNotation)
        {
            infixNotationTokens = infixNotation;
            orderPrecedence.Add("||", 0);
            orderPrecedence.Add("&&", 1);
            orderPrecedence.Add("==", 2);
            orderPrecedence.Add("!=", 2);
            orderPrecedence.Add("<", 3);
            orderPrecedence.Add(">", 3);
            orderPrecedence.Add(">=", 3);
            orderPrecedence.Add("<=", 3);
            orderPrecedence.Add("+", 4);
            orderPrecedence.Add("-", 4);
            orderPrecedence.Add("*", 5);
            orderPrecedence.Add("/", 5);
            orderPrecedence.Add("%", 5);
            orderPrecedence.Add("!", 6);
            orderPrecedence.Add("(", 7);
            orderPrecedence.Add(")", 7);
        }

        public List<string> convert()
        {
            foreach (var token in infixNotationTokens)
            {
                addInfixTokenToPostfixStructure(token);
            }

            addAllNonBracketOperandsToPostFixCollection();
            return postFixNotationTokens;
        }

        private void addInfixTokenToPostfixStructure(string token)
        {
            if (!new OperandUtils().isTokenOperand(token))
            {
                postFixNotationTokens.Add(token);
            }
            else
            {
                addOperandToOperatorStack(token);
            }
        }

        private void addOperandToOperatorStack(string token)
        {
            while (operandStack.Count > 0 && precedenceIsHigherThanOperatorStackPeek(token))
            {
                addNonBracketTopOperandToPostFixTokenCollection();
            }
            operandStack.Push(token);
            if (operandStack.Peek() == ")")
            {
                addAllNonBracketOperandsToPostFixCollection();
            }
        }

        private bool precedenceIsHigherThanOperatorStackPeek(string token)
        {
            return elevateOperatorStackPeekPrecedenceIfOpeningParanthesisIsFound() >= orderPrecedence[token];
        }

        private int elevateOperatorStackPeekPrecedenceIfOpeningParanthesisIsFound()
        {
            return operandStack.Peek() == "("
                ? orderPrecedence[operandStack.Peek()] - 100
                : orderPrecedence[operandStack.Peek()];
        }

        private void addAllNonBracketOperandsToPostFixCollection()
        {
            while (operandStack.Count > 0)
            {
                addNonBracketTopOperandToPostFixTokenCollection();
            }
        }

        private void addNonBracketTopOperandToPostFixTokenCollection()
        {
            var lastOperandOnStack = operandStack.Pop();
            if (lastOperandOnStack == "(" || lastOperandOnStack == ")")
            {
            }
            else
            {
                postFixNotationTokens.Add(lastOperandOnStack);
            }
        }
    }
}