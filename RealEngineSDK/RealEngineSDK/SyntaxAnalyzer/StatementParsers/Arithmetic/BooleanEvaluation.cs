﻿using System.Collections.Generic;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class BooleanSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        public BooleanSyntaxParser() : base(new BooleanOperation())
        {
            var booleanOperation = (BooleanOperation) operation;
            addParsers(new EvaluateLeftOperand(new isValidBooleanType(), booleanOperation));
            addParsers(new EvaluateBooleanSymbol(booleanOperation));
            addParsers(new EvaluateRightOperand(new isValidBooleanType(), operation));
        }


        protected override List<string> preIterateModifier(List<string> tokens)
        {
            if (isNegatedToken(tokens))
            {
                return transformNegetationTokens(tokens[0]);
            }
            return tokens;
        }

        private bool isNegatedToken(List<string> tokens)
        {
            return tokens[0].Length > 1 && tokens[0][0] == '!';
        }

        private List<string> transformNegetationTokens(string negetationToken)
        {
            var secondaryTokenCollection = negetationToken.Split('!');
            var newTokens = new List<string>();
            for (var index = 0; index < 3; index++)
            {
                newTokens.Add(index%2 == 0 ? secondaryTokenCollection[1] : "!");
            }

            return newTokens;
        }
    }
}