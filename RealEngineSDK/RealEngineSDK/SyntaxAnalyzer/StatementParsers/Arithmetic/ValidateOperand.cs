﻿namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public interface ValidateOperand
    {
        bool isValidOperand(string token);
    }
}