﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class EvaluateRightOperand : SymbolParser
    {
        private readonly object rightOperand;
        private readonly ValidateOperand validateOperand;

        public EvaluateRightOperand(ValidateOperand validate, object rightOperand)
        {
            validateOperand = validate;
            this.rightOperand = rightOperand;
        }

        public void onEvaluateSymbol(string token)
        {
            var utils = new OperandUtils();
            if (validateOperand.isValidOperand(token))
            {
                utils.setRightOperand(rightOperand, token);
            }
            else
                throw new InvalidOperandException();
        }
    }
}