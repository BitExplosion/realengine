﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class EvaluateArithmeticType : SymbolParser
    {
        private readonly ArithmeticOperation arithmetic;

        public EvaluateArithmeticType(ArithmeticOperation arithmetic)
        {
            this.arithmetic = arithmetic;
        }

        public void onEvaluateSymbol(string token)
        {
            arithmetic.ArtihmeticType = token;
        }
    }
}