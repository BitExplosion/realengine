﻿using System;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class OperandAttribute : Attribute
    {
        public string operand;
    }
}