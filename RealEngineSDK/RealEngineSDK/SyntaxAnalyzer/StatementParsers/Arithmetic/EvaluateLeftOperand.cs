﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    internal class EvaluateLeftOperand : SymbolParser
    {
        private readonly ValidateOperand validateOperand;
        private readonly object validOperand;

        public EvaluateLeftOperand(ValidateOperand validate, object validOperand)
        {
            validateOperand = validate;
            this.validOperand = validOperand;
        }


        public void onEvaluateSymbol(string token)
        {
            var utils = new OperandUtils();
            if (validateOperand.isValidOperand(token))
            {
                utils.setLeftOperand(validOperand, token);
            }
            else
                throw new InvalidOperandException();
        }
    }
}