﻿using System.Collections.Generic;
using System.Linq;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class OperandUtils
    {
        private readonly List<string> operands = new List<string>
        {
            "+",
            "-",
            "*",
            "/",
            "(",
            ")",
            "&&",
            "||",
            ">",
            "<",
            ">=",
            "<=",
            "%",
            "==",
            "!="
        };

        public bool isTokenOperand(string token)
        {
            return operands.FirstOrDefault(queryToken => queryToken == token) != null;
        }

        public void setLeftOperand(object operation, string token)
        {
            if (operation is ArithmeticOperation)
            {
                var arithmetic = (ArithmeticOperation) operation;
                arithmetic.LeftOperand = token;
            }
            else if (operation is BooleanOperation)
            {
                var booleanOperation = (BooleanOperation) operation;
                booleanOperation.LeftOperand = token;
            }
        }

        public void setRightOperand(object operation, string token)
        {
            if (operation is ArithmeticOperation)
            {
                var arithmetic = (ArithmeticOperation) operation;
                arithmetic.RightOperand = token;
            }
            else if (operation is BooleanOperation)
            {
                var booleanOperation = (BooleanOperation) operation;
                booleanOperation.RightOperand = token;
            }
        }
    }
}