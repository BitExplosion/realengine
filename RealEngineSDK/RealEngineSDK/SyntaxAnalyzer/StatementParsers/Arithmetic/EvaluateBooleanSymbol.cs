﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class EvaluateBooleanSymbol : SymbolParser
    {
        private readonly BooleanOperation boolean;

        public EvaluateBooleanSymbol(BooleanOperation boolean)
        {
            this.boolean = boolean;
        }

        public void onEvaluateSymbol(string token)
        {
            if (token == "isGreaterThan")
            {
                boolean.Type = BooleanOperation.isGreaterThan;
            }
            else if (token == "isLessThan")
            {
                boolean.Type = BooleanOperation.isLesserThan;
            }
            else if (token == "isEqualTo")
            {
                boolean.Type = BooleanOperation.isEqualTo;
            }
            else if (token == "isGreaterThanOrEqualsTo")
            {
                boolean.Type = BooleanOperation.isGreaterThanOrEqualsTo;
            }
            else if (token == "isLessThanOrEqualsTo")
            {
                boolean.Type = BooleanOperation.isLessThanOrEqualsTo;
            }
            else if (token == "!")
            {
                boolean.Type = BooleanOperation.Negation;
            }
        }
    }
}