﻿using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    [Operation]
    public class BooleanOperation
    {
        public static readonly int BOOLEANOPERATIONKEY = 6;

        public BooleanOperation()
        {
            OperationDetails = new OperationDetails(BOOLEANOPERATIONKEY);
        }

        public int Type { get; set; }

        [Operand]
        public string LeftOperand { get; set; }

        [Operand]
        public string RightOperand { get; set; }

        public OperationDetails OperationDetails { get; set; }

        #region booleanTypes

        public static int isGreaterThan = 0;
        public static int isLesserThan = 1;
        public static int isEqualTo = 2;
        public static int isGreaterThanOrEqualsTo = isGreaterThan & isEqualTo;
        public static int isLessThanOrEqualsTo = isLesserThan & isEqualTo;
        public static int Negation = 3;

        #endregion
    }
}