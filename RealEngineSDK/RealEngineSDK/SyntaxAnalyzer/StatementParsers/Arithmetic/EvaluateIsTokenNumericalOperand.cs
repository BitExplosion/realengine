﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.Arithmetic
{
    public class EvaluateValidOperationSymbol : SymbolParser
    {
        private readonly ArithmeticOperation operation;

        public EvaluateValidOperationSymbol(ArithmeticOperation operation)
        {
            this.operation = operation;
        }

        public void onEvaluateSymbol(string token)
        {
            var operandUtilities = new OperandUtils();
            if (operandUtilities.isTokenOperand(token))
            {
                operation.Symbols.Add(token);
            }
            else
                throw new SyntaxAnalyzerException("INVALID SYMBOL FOR OPERATION");
        }
    }
}