﻿using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.Arithmetic
{
    public class EvaluateOperand : SymbolParser
    {
        private readonly ArithmeticOperation arithmetic;

        public EvaluateOperand(ArithmeticOperation arithemticOperation)
        {
            arithmetic = arithemticOperation;
        }

        public void onEvaluateSymbol(string token)
        {
            var utils = new OperandUtils();
            var validateOperand = new IsValidNumericalOperand();
            if (token == "end" || (validateOperand.isValidOperand(token) && !TokenIdentifier.isTokenReserved(token)))
            {
                arithmetic.Symbols.Add(token);
            }
            else
                throw new InvalidOperandException();
        }
    }
}