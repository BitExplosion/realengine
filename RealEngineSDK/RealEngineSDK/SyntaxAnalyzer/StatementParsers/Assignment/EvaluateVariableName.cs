﻿using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Assignment
{
    public class EvaluateVariableName : SymbolParser
    {
        private readonly AssignmentOperation assignment;

        public EvaluateVariableName(AssignmentOperation assignment)
        {
            this.assignment = assignment;
        }

        public void onEvaluateSymbol(string token)
        {
            if (token != "")
            {
                setCreationOperationVariableName(assignment, token);
            }
        }

        private void setCreationOperationVariableName(AssignmentOperation operation, string token)
        {
            var assignment = operation;
            if (!TokenIdentifier.isVariableCreationKeywordValid(token))
            {
                assignment.Variable = token;
            }

            if (TokenIdentifier.isThisAVariabelType(token))
            {
                throw new SyntaxAnalyzerException("CANNOT ASSIGN TO RESERVED: " + token);
                ;
            }
        }
    }
}