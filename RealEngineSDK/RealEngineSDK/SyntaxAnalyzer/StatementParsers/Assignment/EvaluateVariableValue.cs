﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Assignment
{
    public class EvaluateVariableValue : SymbolParser
    {
        private readonly AssignmentOperation assignment;

        public EvaluateVariableValue(AssignmentOperation assignment)
        {
            this.assignment = assignment;
        }

        public void onEvaluateSymbol(string token)
        {
            assignment.Value = token;
        }
    }
}