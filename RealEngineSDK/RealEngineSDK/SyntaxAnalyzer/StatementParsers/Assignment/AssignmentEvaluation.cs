﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Assignment;
using RealEngineSDK.Utility;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class AssignmentSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        private readonly SymbolTableIterator arithmeticParser;
        private readonly AssignmentOperation assignmentOperation;

        public AssignmentSyntaxParser(SymbolTableIterator arithmeticParser) : base(new AssignmentOperation())
        {
            assignmentOperation = (AssignmentOperation) operation;
            addParsers(new EvaluateVariableName(assignmentOperation));
            addParsers(new EvaluateAssignmentStatement());
            this.arithmeticParser = arithmeticParser;
        }

        protected override List<string> preIterateModifier(List<string> tokens)
        {
            var engine = new TokenQueryEngine(tokens);
            var assignCodeSegment = engine.queryAssignCode();
            var arithmetic = (ArithmeticOperation) arithmeticParser.evaluateLineOfCode(assignCodeSegment);
            if (arithmetic != null)
            {
                assignmentOperation.Arithmetic = arithmetic;
            }
            else
            {
                throw new SyntaxAnalyzerException("COULD NOT PARSE ARITHMETIC OPERATION");
            }
            if (tokens.Count > 2)
                return tokens.GetRange(0, 2);
            throw new SyntaxAnalyzerException("INCOMPLETE ASSIGNMENT OPERATION");
        }
    }
}