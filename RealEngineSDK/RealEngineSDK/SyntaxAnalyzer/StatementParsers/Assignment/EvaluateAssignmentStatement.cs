﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Assignment
{
    public class EvaluateAssignmentStatement : SymbolParser
    {
        public void onEvaluateSymbol(string token)
        {
            if (token != "assign")
            {
                throw new SyntaxAnalyzerException(token + " : VARIABLE IS MISSING ASSIGNMENT TYPE");
            }
        }
    }
}