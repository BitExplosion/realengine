﻿namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable
{
    public interface SymbolParser
    {
        void onEvaluateSymbol(string token);
    }
}