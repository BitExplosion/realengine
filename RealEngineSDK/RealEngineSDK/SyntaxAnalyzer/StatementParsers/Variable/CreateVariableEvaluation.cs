﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class CreateVariableSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        public CreateVariableSyntaxParser() : base(new CreateVariableOperation())
        {
            var createVariableOperation = (CreateVariableOperation) operation;
            addParsers(new VariableSetByTypeParser(createVariableOperation));
            addParsers(new VariableNameSetByParser(createVariableOperation));
        }

        protected override List<string> preIterateModifier(List<string> tokens)
        {
            return tokens;
        }

        protected override void onIterate(string token)
        {
            Parsers[symbolPosition].onEvaluateSymbol(token);
            symbolPosition++;
        }
    }
}