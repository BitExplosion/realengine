﻿using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable
{
    public class VariableNameSetByParser : SymbolParser
    {
        private CreateVariableOperation variable;


        public VariableNameSetByParser(CreateVariableOperation operation)
        {
            variable = operation;
        }


        public void onEvaluateSymbol(string token)
        {
            if (!TokenIdentifier.isTokenReserved(token))
            {
                variable = evaluateCreateVariableOperation(token);
            }
            else
            {
                throw new SyntaxAnalyzerException("CANNOT USE TYPE AS VARIABLE NAME");
            }
        }

        private CreateVariableOperation evaluateCreateVariableOperation(string token)
        {
            if (!TokenIdentifier.isTokenReserved(token))
            {
                setCreateOperationVariableName(variable, token);
            }
            else
            {
                throw new SyntaxAnalyzerException("CANNOT USE TYPE AS VARIABLE NAME");
            }

            return variable;
        }


        private void setCreateOperationVariableNameWithToken(CreateVariableOperation variable, string token)
        {
            variable.VariableName = token;
        }

        private CreateVariableOperation setCreateOperationVariableName(CreateVariableOperation variable, string token)
        {
            setCreateOperationVariableNameWithToken(variable, token);
            return variable;
        }
    }
}