﻿namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable
{
    public class VariableSetByTypeParser : SymbolParser
    {
        private readonly CreateVariableOperation variable;

        public VariableSetByTypeParser(CreateVariableOperation variable)
        {
            this.variable = variable;
        }

        public void onEvaluateSymbol(string token)
        {
            setVariableType(variable, token);
        }

        private void setVariableOperationTokenWithToken(CreateVariableOperation operation, string token)
        {
            variable.setVariableOperationVariableTypeByOperation(token);
        }

        private void setVariableType(CreateVariableOperation operation, string token)
        {
            setVariableOperationTokenWithToken(operation, token);
        }
    }
}