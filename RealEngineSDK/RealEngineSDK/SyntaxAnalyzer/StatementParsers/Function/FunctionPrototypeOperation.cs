﻿using System.Collections.Generic;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;

namespace RealEngineSDK.SyntaxAnalyzer.Operations
{
    [Operation]
    public class FunctionPrototypeOperation
    {
        public static readonly int FUNCTIONPROTOTYPEKEY = 4;

        public FunctionPrototypeOperation()
        {
            Properties = new List<string>();
            OperationDetails = new OperationDetails(FUNCTIONPROTOTYPEKEY);
            Arguments = new List<CreateVariableOperation>();
        }

        public int ReturnType { get; set; }

        public string FunctionName { get; set; }

        public List<string> Properties { get; set; }

        public List<CreateVariableOperation> Arguments { get; set; }

        public OperationDetails OperationDetails { get; set; }


        public void setTokenToReturnType(string token)
        {
            ReturnType = TokenIdentifier.convertFromTokenToReturnType(token);
        }


        public void addArguments(CreateVariableOperation argument)
        {
            Arguments.Add(argument);
        }
    }
}