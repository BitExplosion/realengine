﻿using RealEngineSDK.SyntaxAnalyzer.Operations;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Function.FunctionPrototype
{
    public class MethodNameParser : SymbolParser
    {
        private readonly FunctionPrototypeOperation functionPrototype;

        public MethodNameParser(FunctionPrototypeOperation functionPrototype)
        {
            this.functionPrototype = functionPrototype;
        }

        public void onEvaluateSymbol(string token)
        {
            functionPrototype.FunctionName = token;
        }
    }
}