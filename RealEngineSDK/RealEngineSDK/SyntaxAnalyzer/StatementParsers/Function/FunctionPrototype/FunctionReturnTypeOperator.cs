﻿using RealEngineSDK.SyntaxAnalyzer.Operations;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Function.FunctionPrototype
{
    public class FunctionReturnTypeOperator : SymbolParser
    {
        private readonly FunctionPrototypeOperation functionPrototype;

        public FunctionReturnTypeOperator(FunctionPrototypeOperation functionPrototype)
        {
            this.functionPrototype = functionPrototype;
        }

        public void onEvaluateSymbol(string token)
        {
            functionPrototype.setTokenToReturnType(token);
        }
    }
}