﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Function.FunctionPrototype
{
    public class WithParser : SymbolParser
    {
        public void onEvaluateSymbol(string token)
        {
            if (token != "With")
            {
                throw new SyntaxAnalyzerException("Invalid With Keyword for method declaration");
            }
        }
    }
}