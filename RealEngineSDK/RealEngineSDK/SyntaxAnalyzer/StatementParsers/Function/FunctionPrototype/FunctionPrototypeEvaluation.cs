﻿using System.Collections.Generic;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.Operations;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Function.FunctionPrototype;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser
{
    public class FunctionPrototypeSyntaxParser : SymbolTableIterator, SymbolSyntaxParser
    {
        private readonly ArgumentParser argumentParser;

        public FunctionPrototypeSyntaxParser() : base(new FunctionPrototypeOperation())
        {
            var functionPrototypeOperation = (FunctionPrototypeOperation) operation;
            addParsers(new FunctionReturnTypeOperator(functionPrototypeOperation));
            addParsers(new MethodNameParser(functionPrototypeOperation));
            addParsers(new WithParser());
            argumentParser = new ArgumentParser(functionPrototypeOperation);
        }

        protected override List<string> preIterateModifier(List<string> tokens)
        {
            return tokens;
        }

        protected override void onIterate(string token)
        {
            var create = (FunctionPrototypeOperation) operation;
            if (TokenIdentifier.isVariableCreationKeywordValid(token))
            {
                // create.addProperties(token);
                return;
            }
            if (symbolPosition >= 2)
            {
                addParsers(argumentParser);
            }
            Parsers[symbolPosition].onEvaluateSymbol(token);
            symbolPosition++;
            if (token == "end")
            {
                removeParser(argumentParser);
            }
        }
    }
}