﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.Operations;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Variable;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Function.FunctionPrototype
{
    internal class ArgumentParser : SymbolParser
    {
        private readonly List<string> argumentCreator = new List<string>();
        private List<string> arguments = new List<string>();
        private readonly CreateVariableSyntaxParser createVariableOperation = new CreateVariableSyntaxParser();
        private readonly FunctionPrototypeOperation functionPrototype;

        public ArgumentParser(FunctionPrototypeOperation functionPrototype)
        {
            this.functionPrototype = functionPrototype;
        }

        public void onEvaluateSymbol(string token)
        {
            if (token == "Nothing")
            {
            }
            else
            {
                if (isThisEndOfSyntaxParser(token))
                {
                    argumentCreator.Add(token);
                }
                else
                {
                    if (argumentCreator.Count != 0)
                    {
                        var functionArgument =
                            (CreateVariableOperation) createVariableOperation.evaluateLineOfCode(argumentCreator);
                        functionPrototype.Arguments.Add(functionArgument);
                        argumentCreator.Clear();
                    }
                }
            }
        }

        private bool isThisEndOfSyntaxParser(string token)
        {
            return (token != "With") && (token != "end");
        }
    }
}