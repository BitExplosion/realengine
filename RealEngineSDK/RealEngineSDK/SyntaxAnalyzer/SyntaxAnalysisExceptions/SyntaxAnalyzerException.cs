﻿using System;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions
{
    public class SyntaxAnalyzerException : Exception
    {
        private Type type;

        public SyntaxAnalyzerException(string message) : base(message)
        {
        }
    }
}