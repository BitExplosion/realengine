﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General;

namespace RealEngineSDK.SyntaxAnalyzer
{
    public class DeclarationAssignmentChainedOperation
    {
        public static int DECLARATIONASSIGNMENTCHAINEDKEY = Operation.NONOPERATION |
                                                            AssignmentOperation.ASSIGNMENTOPERATIONKEY;

        public DeclarationAssignmentChainedOperation()
        {
            OperationDetails = new OperationDetails(DECLARATIONASSIGNMENTCHAINEDKEY);
        }

        public DeclarationAssignmentChainedOperation(CreateVariableOperation create, AssignmentOperation assignment)
        {
            Assignment = Assignment;
            Create = create;
            OperationDetails = new OperationDetails(DECLARATIONASSIGNMENTCHAINEDKEY);
        }

        public CreateVariableOperation Create { get; set; }

        public OperationDetails OperationDetails { get; set; }

        public AssignmentOperation Assignment { get; set; }
    }
}