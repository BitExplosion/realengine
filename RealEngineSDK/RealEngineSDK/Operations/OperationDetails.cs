﻿namespace RealEngineSDK.SyntaxAnalyzer
{
    public class OperationDetails
    {
        public OperationDetails(int operation)
        {
            OperationType = operation;
        }


        public int OperationType { get; set; }
    }
}