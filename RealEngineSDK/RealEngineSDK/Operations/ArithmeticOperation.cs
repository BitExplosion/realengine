﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    [Operation]
    public class ArithmeticOperation
    {
        public static int ARITHMETICOPERATIONKEY = 9;
        private OperationDetails operationDetails;

        public ArithmeticOperation()
        {
            operationDetails = new OperationDetails(ARITHMETICOPERATIONKEY);
            Symbols = new List<string>();
        }

        public List<string> Symbols { get; set; }

        public int Result { get; set; } = 0;

        [Operand]
        public string LeftOperand { get; set; }

        [Operand]
        public string RightOperand { get; set; }

        public string ArtihmeticType { get; set; }
    }
}