﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.SyntaxAnalyzer.StatementParsers.WhenStatement
{
    [Operation]
    public class WhenStatementOperation
    {
        public List<object> blockOperations;

        public WhenStatementOperation()
        {
            blockOperations = new List<object>();
        }

        public ArithmeticOperation WhenCondition { get; set; }

        public List<object> BlockOperations { get; set; }
    }
}