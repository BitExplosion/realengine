﻿using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.SyntaxAnalyzer
{
    [Operation]
    public class AssignmentOperation
    {
        public static readonly int ASSIGNMENTOPERATIONKEY = 2;

        public AssignmentOperation()
        {
            OperationDetails = new OperationDetails(ASSIGNMENTOPERATIONKEY);
            Arithmetic = new ArithmeticOperation();
        }


        public ArithmeticOperation Arithmetic { get; set; }

        public string Variable { get; set; }

        public string Value { get; set; }

        public OperationDetails OperationDetails { get; set; }
    }
}