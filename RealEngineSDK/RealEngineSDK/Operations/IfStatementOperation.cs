﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement
{
    [Operation]
    public class IfStatementOperation
    {
        public static int IFSTATEMENTOPERATIONKEY = 15;
        public List<object> blockOperations;

        public IfStatementOperation()
        {
            OperationDetails = new OperationDetails(IFSTATEMENTOPERATIONKEY);
            Condition = new ArithmeticOperation();
            blockOperations = new List<object>();
            ElseIfConditions = new List<IfStatementOperation>();
        }

        public ArithmeticOperation Condition { get; set; }

        public List<IfStatementOperation> ElseIfConditions { get; }

        public IfStatementOperation ElseCondition { get; set; }

        public OperationDetails OperationDetails { get; set; }

        public List<object> BlockOperations
        {
            get { return blockOperations; }
            set { blockOperations = value; }
        }
    }
}