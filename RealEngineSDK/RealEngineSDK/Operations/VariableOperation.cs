﻿using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.General;

namespace RealEngineSDK.SyntaxAnalyzer
{
    [Operation]
    public class CreateVariableOperation
    {
        public static readonly int CREATEVARIABLE = 1;

        public CreateVariableOperation()
        {
            VariableType = -1;
            VariableName = "";
            OperationDetails = new OperationDetails(CREATEVARIABLE);
        }

        public CreateVariableOperation(string variableName)
        {
            VariableName = variableName;
            OperationDetails = new OperationDetails(CREATEVARIABLE);
        }


        public string VariableName { get; set; }

        public int VariableType { get; set; }

        public OperationDetails OperationDetails { get; set; }

        public void setVariableOperationVariableTypeByOperation(string token)
        {
            VariableType = TokenIdentifier.convertFromTokenToReturnType(token);
        }
    }
}