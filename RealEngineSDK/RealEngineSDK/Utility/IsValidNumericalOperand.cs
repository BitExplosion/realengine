﻿using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic
{
    public class IsValidNumericalOperand : ValidateOperand
    {
        public bool isValidOperand(string token)
        {
            return !(token.Contains("\"") || TokenIdentifier.isThisAVariabelType(token)) &&
                   tokenIsNotArithmeticOperation(token);
        }

        private bool tokenIsNotArithmeticOperation(string token)
        {
            return !new OperandUtils().isTokenOperand(token);
        }
    }
}