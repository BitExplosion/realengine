﻿namespace RealEngineSDK.Utility.TokenQueryCollection
{
    public class IfStatementReservedWordTokenQueryCollection : ListTokenQueryCollection
    {
        public override void add(string token)
        {
            if (tokenIsReservedKeyword(token))
            {
                getTokenCollectionAsList().Add(token);
            }
        }

        private bool tokenIsReservedKeyword(string token)
        {
            return token == "if" || token == "then" || token == "end" || token == "else" || token == "elseif";
        }
    }
}