﻿using System.Collections.Generic;

namespace RealEngineSDK.Utility.TokenQueryCollection
{
    public class CodeSegmentTokenQueryCollection : ListTokenQueryCollection
    {
        private readonly List<List<string>> codeSegments = new List<List<string>>();
        private int codeSegmentsRecorded;
        private readonly string endOfSegmentIdentifier;
        private bool isValidCodeSegment;

        private readonly List<string> startOfSegmentIdentifiers;

        public CodeSegmentTokenQueryCollection(List<string> startOfSegmentIdentifiers, string endofSegmentIdentifier)
        {
            this.startOfSegmentIdentifiers = startOfSegmentIdentifiers;
            endOfSegmentIdentifier = endofSegmentIdentifier;
        }


        public override void add(string token)
        {
            isValidCodeSegment = isTokenInCodeSegment(token);
            if (isValidCodeSegment && isTokenNotCodeSegmentIdentifier(token))
            {
                codeSegments[codeSegmentsRecorded].Add(token);
            }
        }

        private bool isTokenNotCodeSegmentIdentifier(string token)
        {
            if (!isTokenStartCodeSegmentIdentifier(token))
            {
                return true;
            }
            return !(token != endOfSegmentIdentifier);
        }


        public List<string> getBlockOfCodeByIndex(int codeSegmentIndex)
        {
            return codeSegmentIndex < codeSegments.Count ? codeSegments[codeSegmentIndex] : new List<string>();
        }

        private bool isTokenStartCodeSegmentIdentifier(string token)
        {
            foreach (var startCodeSegmentIdentifier in startOfSegmentIdentifiers)
            {
                if (startCodeSegmentIdentifier == token)
                    return true;
            }
            return false;
        }

        private bool isTokenInCodeSegment(string token)
        {
            if (isTokenStartCodeSegmentIdentifier(token))
            {
                isValidCodeSegment = true;
                codeSegments.Add(new List<string>());
            }

            if (token == endOfSegmentIdentifier)
            {
                isValidCodeSegment = false;
                codeSegmentsRecorded++;
            }

            return isValidCodeSegment;
        }
    }
}