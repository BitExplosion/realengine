﻿using System.Collections.Generic;

namespace RealEngineSDK.Utility.TokenQueryCollection
{
    public abstract class ListTokenQueryCollection : TokenQueryCollection
    {
        private readonly List<string> tokenList;

        public ListTokenQueryCollection()
        {
            tokenList = new List<string>();
        }

        public abstract void add(string token);


        public List<string> getTokenCollectionAsList()
        {
            return tokenList;
        }
    }
}