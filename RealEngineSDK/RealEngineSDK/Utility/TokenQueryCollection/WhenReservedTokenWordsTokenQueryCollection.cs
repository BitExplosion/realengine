﻿namespace RealEngineSDK.Utility.TokenQueryCollection
{
    public class WhenReservedTokenWordsTokenQueryCollection : ListTokenQueryCollection
    {
        public override void add(string token)
        {
            if (tokenIsReservedKeyword(token))
            {
                getTokenCollectionAsList().Add(token);
            }
        }

        private bool tokenIsReservedKeyword(string token)
        {
            return token == "end" || token == "when" || token == "then";
        }
    }
}