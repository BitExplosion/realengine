﻿namespace RealEngineSDK.Utility.TokenQueryCollection
{
    internal interface TokenQueryCollection
    {
        void add(string token);
    }
}