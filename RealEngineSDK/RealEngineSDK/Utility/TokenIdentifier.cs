﻿using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;

namespace RealEngineSDK.LexicalAnalyzer.Utility
{
    public class TokenIdentifier
    {
        public static readonly int CREATEVARIABLE = 1;
        public static readonly int INTTYPE = 0;
        public static readonly int FLOATTYPE = 1;
        public static readonly int CHARTYPE = 2;
        public static readonly int STRINGTYPE = 3;

        public static bool isVariableCreationKeywordValid(string token)
        {
            string[] keywords = {"public", "private", "const", "static", "DEFINE"};
            return iterateThroughArray(keywords, token);
        }

        public static bool tokenIsReservedWord(string token)
        {
            string[] keywords = {"is", "elseif", "else", "then", "end", "with", "when", "."};
            return iterateThroughArray(keywords, token);
        }


        public static bool isThisAVariabelType(string token)
        {
            string[] variablesTypes = {"int", "char", "string", "float"};
            return iterateThroughArray(variablesTypes, token);
        }

        public static bool isTokenReserved(string token)
        {
            return isThisAVariabelType(token) || isVariableCreationKeywordValid(token) || tokenIsReservedWord(token);
        }

        public static int convertFromTokenToReturnType(string token)
        {
            var variableTypes = new Dictionary<string, int>();
            variableTypes.Add("int", INTTYPE);
            variableTypes.Add("float", FLOATTYPE);
            variableTypes.Add("char", CHARTYPE);
            variableTypes.Add("string", STRINGTYPE);
            if (variableTypes.ContainsKey(token))
            {
                return variableTypes[token];
            }
            throw new SyntaxAnalyzerException(token + " : PRIMITIVE DATA TYPE OR STRUCT IS NOT SUPPORTED");
        }


        private static bool iterateThroughArray(string[] arrayContent, string token)
        {
            for (var valueFromSet = 0; valueFromSet < arrayContent.Length; valueFromSet++)
            {
                if (arrayContent[valueFromSet] == token)
                {
                    return true;
                }
            }

            return false;
        }
    }
}