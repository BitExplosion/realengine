﻿using System.Collections.Generic;
using RealEngineSDK.Utility.TokenQueryCollection;

namespace RealEngineSDK.Utility
{
    public class TokenQueryEngine
    {
        private ListTokenQueryCollection _assignmentTokens;
        private ListTokenQueryCollection _blockTokenQueryTokens;
        private ListTokenQueryCollection _ifStatmentTokens;
        private int blockCodeQueryCounter;
        private int ifStatmentQueryCounter;
        private readonly List<string> tokenList;

        public TokenQueryEngine(List<string> tokenList)
        {
            this.tokenList = tokenList;
            initializeBlockCodeQuery();
            initializeIfStatementCodeQuery();
            initializeAssignStatementCodeQuery();
        }

        private void initializeBlockCodeQuery()
        {
            var blockCodeStartIdentifiers = new List<string>();
            blockCodeStartIdentifiers.Add("then");
            _blockTokenQueryTokens = new CodeSegmentTokenQueryCollection(blockCodeStartIdentifiers, "end");
            _blockTokenQueryTokens = queryTokensByListQueryCollection(_blockTokenQueryTokens);
        }

        private void initializeIfStatementCodeQuery()
        {
            var ifStatementStartIdentifiers = new List<string>();
            ifStatementStartIdentifiers.Add("if");
            ifStatementStartIdentifiers.Add("elseif");
            _ifStatmentTokens = new CodeSegmentTokenQueryCollection(ifStatementStartIdentifiers, "then");
            _ifStatmentTokens = queryTokensByListQueryCollection(_ifStatmentTokens);
        }

        private void initializeAssignStatementCodeQuery()
        {
            var _assignmentStatementStartIdentifiers = new List<string>();
            _assignmentStatementStartIdentifiers.Add("assign");
            _assignmentTokens = new CodeSegmentTokenQueryCollection(_assignmentStatementStartIdentifiers, "");
            _assignmentTokens = queryTokensByListQueryCollection(_assignmentTokens);
        }

        private ListTokenQueryCollection queryTokensByListQueryCollection(ListTokenQueryCollection tokenQueryCollection)
        {
            foreach (var token in tokenList)
            {
                tokenQueryCollection.add(token);
            }
            return tokenQueryCollection;
        }

        public List<string> queryNextIfBooleanStatementTokens()
        {
            var queryTokenQueryCollection = (CodeSegmentTokenQueryCollection) _ifStatmentTokens;
            var ifStatementCodeSegment = queryTokenQueryCollection.getBlockOfCodeByIndex(ifStatmentQueryCounter);
            ifStatmentQueryCounter++;
            return ifStatementCodeSegment;
        }

        public List<string> queryNextBlockOfCode()
        {
            var tokenQueryCollection = (CodeSegmentTokenQueryCollection) _blockTokenQueryTokens;
            var blockCode = tokenQueryCollection.getBlockOfCodeByIndex(blockCodeQueryCounter);
            blockCodeQueryCounter++;
            return blockCode;
        }

        public List<string> queryAssignCode()
        {
            var tokenQueryCollection = (CodeSegmentTokenQueryCollection) _assignmentTokens;
            return tokenQueryCollection.getBlockOfCodeByIndex(0);
        }

        public List<string> queryWhenCondition()
        {
            var whenStatementStartIdentifiers = new List<string>();
            whenStatementStartIdentifiers.Add("when");
            ListTokenQueryCollection whenStatementCodeSegmentTokenQueryCollection =
                new CodeSegmentTokenQueryCollection(whenStatementStartIdentifiers, "then");
            whenStatementCodeSegmentTokenQueryCollection =
                queryTokensByListQueryCollection(whenStatementCodeSegmentTokenQueryCollection);
            var whenConditionCodeSegment =
                (CodeSegmentTokenQueryCollection) whenStatementCodeSegmentTokenQueryCollection;
            return whenConditionCodeSegment.getBlockOfCodeByIndex(0);
        }


        public List<string> queryReservedWordTokens()
        {
            return
                queryTokensByListQueryCollection(new IfStatementReservedWordTokenQueryCollection())
                    .getTokenCollectionAsList();
        }

        public List<string> queryReservedWhenWordTokens()
        {
            return
                queryTokensByListQueryCollection(new WhenReservedTokenWordsTokenQueryCollection())
                    .getTokenCollectionAsList();
        }
    }
}