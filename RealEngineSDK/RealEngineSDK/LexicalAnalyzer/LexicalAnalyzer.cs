﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace RealEngineSDK.LexicalAnalyzer
{
    public class LexicalAnalyzer
    {
        private bool isEndOfLineOfCode(string lineOfCode)
        {
            return lineOfCode.Length == 0 || lineOfCode == ".";
        }

        private List<string> createListWithEmptyString()
        {
            var listWithEmptyStringOnly = new List<string>();
            listWithEmptyStringOnly.Add("");
            return listWithEmptyStringOnly;
        }


        public List<string> tokenizeLine(string lineOfCode)
        {
            var delimiter = ".";
            lineOfCode = lineOfCode.Replace(delimiter, "");
            if (isEndOfLineOfCode(lineOfCode))
            {
                return createListWithEmptyString();
            }
            var tokens = lineOfCode.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
            return tokens.Select(token => token.Replace(".", "")).ToList();
        }

        public List<List<string>> tokenize(string sourceCode)
        {
            sourceCode = removeComments(sourceCode);
            string[] singleStatementDelimiter = {"."};
            var nonTokenizedLinesOfCode = sourceCode.Split(singleStatementDelimiter,
                StringSplitOptions.RemoveEmptyEntries);
            return nonTokenizedLinesOfCode.Select(tokenizeLine).ToList();
        }

        private string removeComments(string sourceCodeWithComments)
        {
            var regexToIgnoreComments = @"\\.*(\r\n|\r|\n)";
            return Regex.Replace(sourceCodeWithComments, regexToIgnoreComments, string.Empty);
        }
    }
}