﻿namespace RealEngineSDK.Code_Generation.OperationExecuters
{
    public abstract class OperationExecuter
    {

        public string executeOperation(string leftOperand, string rightOperand)
        {
            OperandHelper variableTypeIdentifier = new OperandHelper();
            if (variableTypeIdentifier.isOperandFloat(leftOperand) || variableTypeIdentifier.isOperandFloat(rightOperand))
            {
                return executeFloatStatement(leftOperand, rightOperand);
            }
            else if((!leftOperand.Contains(".") && !leftOperand.Contains("'")) || (!rightOperand.Contains(".") && !rightOperand.Contains("'")))
            {
                return executeIntStatement(leftOperand, rightOperand);
            }
            else
            {
                return executeCharStatement(leftOperand, rightOperand);
            }
            
        }

        protected abstract string executeIntStatement(string leftOperand, string rightOperand);
        protected abstract string executeFloatStatement(string leftOperand, string rightOperand);

        protected abstract string executeCharStatement(string leftOperand, string rightOperand);
    }
}