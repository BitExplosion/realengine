﻿namespace RealEngineSDK.Code_Generation.OperationExecuters
{
    public class DivisionExecuter : OperationExecuter
    {
        protected override string executeIntStatement(string leftOperand, string rightOperand)
        {
            return (long.Parse(leftOperand) / long.Parse(rightOperand)).ToString();
        }

        protected override string executeFloatStatement(string leftOperand, string rightOperand)
        {
            return (float.Parse(leftOperand) / float.Parse(rightOperand)).ToString();
        }

        protected override string executeCharStatement(string leftOperand, string rightOperand)
        {
            leftOperand = leftOperand.Replace("'", "");
            rightOperand = rightOperand.Replace("'", "");
            return (char.Parse(leftOperand) / char.Parse(rightOperand)).ToString();
        }
    }
}