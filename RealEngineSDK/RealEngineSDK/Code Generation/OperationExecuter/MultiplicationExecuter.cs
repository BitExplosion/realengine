﻿namespace RealEngineSDK.Code_Generation.OperationExecuters
{
    public class MultiplicationExecuter : OperationExecuter
    {
        protected override string executeIntStatement(string leftOperand, string rightOperand)
        {
            return (long.Parse(leftOperand) * long.Parse(rightOperand)).ToString();
        }

        protected override string executeFloatStatement(string leftOperand, string rightOperand)
        {
            return (float.Parse(leftOperand) * float.Parse(rightOperand)).ToString();
        }

        protected override string executeCharStatement(string leftOperand, string rightOperand)
        {
            return (char.Parse(leftOperand) * char.Parse(rightOperand)).ToString();
        }

    }
}