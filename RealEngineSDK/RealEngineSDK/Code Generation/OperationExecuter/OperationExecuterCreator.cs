﻿using System.Collections.Generic;

namespace RealEngineSDK.Code_Generation.OperationExecuters
{
    public class OperationExecuterCreator
    {
        private readonly Dictionary<string, OperationExecuter> keyToDictionaryTypes = new Dictionary
            <string, OperationExecuter>
        {
            {"+", new AdditionExecuter()},
            {"-", new SubtractionExecuter()},
            {"*", new MultiplicationExecuter()},
            {"/", new DivisionExecuter()},
            {"<", new LessThanExecuter()},
            {">", new GreaterThanExecuter()},
            {"<=", new LessThanOrEqualToExecuter()},
            {">=", new GreaterThanOrEqualToExecuter()},
            {"==", new EqualToExecuter()},
            {"!=", new NotEqualExecuter()}
        };

        public OperationExecuter createExecuter(string operatorToCreate)
        {
            return keyToDictionaryTypes[operatorToCreate];
        }
    }
}