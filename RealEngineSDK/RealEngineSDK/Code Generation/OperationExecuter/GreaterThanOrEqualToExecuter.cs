﻿namespace RealEngineSDK.Code_Generation.OperationExecuters
{
    public class GreaterThanOrEqualToExecuter : OperationExecuter
    {
        protected override string executeIntStatement(string leftOperand, string rightOperand)
        {
            return long.Parse(leftOperand) >= long.Parse(rightOperand) ? "1" : "0";
        }

        protected override string executeFloatStatement(string leftOperand, string rightOperand)
        {
            return float.Parse(leftOperand) >= float.Parse(rightOperand) ? "1" : "0";
        }

        protected override string executeCharStatement(string leftOperand, string rightOperand)
        {
            return char.Parse(leftOperand) >= char.Parse(rightOperand) ? "1" : "0";
        }
    }
}