﻿using System;

namespace RealEngineSDK.Code_Generation
{
    public class CodeGenerationException : Exception
    {
        public CodeGenerationException(string message)
            : base(message)
        {
        }
    }
}