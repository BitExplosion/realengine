﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer;

namespace RealEngineSDK.Code_Generation
{
    public class PrimitiveCollection : IEnumerable<CreateVariableOperation>
    {
        private readonly Dictionary<int, int> variableTypeSize = new Dictionary<int, int>
        {
            {TokenIdentifier.INTTYPE, 64},
            {TokenIdentifier.FLOATTYPE, 64},
            {TokenIdentifier.CHARTYPE, 1}
        };

        public PrimitiveCollection(List<CreateVariableOperation> variablesToCreate)
        {
            CreatedVariables = new List<CreateVariableOperation>();
            foreach (var variable in variablesToCreate)
            {
                addNewVariableToCollection(variable);
            }
        }

        public int Count
        {
            get { return CreatedVariables.Count; }
            set { }
        }


        public List<CreateVariableOperation> CreatedVariables { get; set; }

        public IEnumerator<CreateVariableOperation> GetEnumerator()
        {
            foreach (var createdVariablesForEnumarator in CreatedVariables)
            {
                yield return createdVariablesForEnumarator;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int sizeOfVariable(CreateVariableOperation operation)
        {
            return variableTypeSize[operation.VariableType];
        }

        private void addNewVariableToCollection(CreateVariableOperation variable)
        {
            if (CreatedVariables.FirstOrDefault(queryVariable => queryVariable.VariableName == variable.VariableName) ==
                null)
            {
                CreatedVariables.Add(variable);
            }
            else
            {
                throw new CodeGenerationException("VARIABLE WITH THAT NAME ALREADY EXISTS");
            }
        }


        public void Add(CreateVariableOperation variable)
        {
            addNewVariableToCollection(variable);
        }

        public CreateVariableOperation delete(string nameOfVariableToDelete)
        {
            var variableToDelete =
                CreatedVariables.FirstOrDefault(queryVariable => queryVariable.VariableName == nameOfVariableToDelete);
            CreatedVariables.RemoveAll(queryVariable => queryVariable.VariableName == nameOfVariableToDelete);

            if (variableToDelete == null)
            {
                variableToDelete = new CreateVariableOperation();
                variableToDelete.VariableName = "";
            }

            return variableToDelete;
        }

        public int sizeOffset(string nameOfVariableToFind)
        {
            var sumOfVariableTypeSizes = 0;
            foreach (var variableFromCollection in CreatedVariables)
            {
                if (variableFromCollection.VariableName == nameOfVariableToFind)
                {
                    return sumOfVariableTypeSizes;
                }
                sumOfVariableTypeSizes += variableTypeSize[variableFromCollection.VariableType];
            }
            return sumOfVariableTypeSizes;
        }

        public bool contains(string variableToSearchFor)
        {
            return
                CreatedVariables.FirstOrDefault(queryVariable => queryVariable.VariableName == variableToSearchFor) !=
                null;
        }

        public int TotalSizeOfVariables()
        {
            return sizeOffset("");
        }
    }
}