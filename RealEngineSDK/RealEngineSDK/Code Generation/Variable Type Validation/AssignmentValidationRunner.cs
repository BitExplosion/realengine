﻿using RealEngineSDK.SyntaxAnalyzer;

namespace RealEngineSDK.Code_Generation.Variable_Type_Validation
{
    public class AssignmentValidationRunner
    {
        private readonly AssignmentOperation assignmentToValidate;
        private CreateVariableOperation variableToAssignTo;

        public AssignmentValidationRunner(CreateVariableOperation variableToValidate,
            AssignmentOperation assignmentToValidate)
        {
            variableToAssignTo = variableToValidate;
            this.assignmentToValidate = assignmentToValidate;
        }

        public bool validateAllSymbols(IAssignmentSymbolValidator symbolValidator)
        {
            foreach (var arithmetic in assignmentToValidate.Arithmetic.Symbols)
            {
                symbolValidator.validateSymbol(arithmetic);
            }


            return true;
        }
    }
}