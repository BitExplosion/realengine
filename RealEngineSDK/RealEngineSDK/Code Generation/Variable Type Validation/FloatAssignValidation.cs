﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.Code_Generation.Variable_Type_Validation
{
    public class FloatAssignValidation : IAssignmentSymbolValidator
    {
        public void validateSymbol(string arithmetic)
        {
            if (new IsValidNumericalOperand().isValidOperand(arithmetic))
            {
                if (arithmetic.StartsWith("'") && arithmetic.EndsWith("'"))
                {
                    throw new CodeGenerationException("FLOAT CANNOT ACCEPT CHAR");
                }
            }
        }
    }
}