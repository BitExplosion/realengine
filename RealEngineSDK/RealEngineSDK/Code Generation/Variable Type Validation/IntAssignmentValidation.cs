﻿using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.Code_Generation.Variable_Type_Validation
{
    public class IntAssignmentValidation : IAssignmentSymbolValidator
    {
        public void validateSymbol(string arithmetic)
        {
            if (new IsValidNumericalOperand().isValidOperand(arithmetic))
            {
                long resultToSaveTo;
                if (!long.TryParse(arithmetic, out resultToSaveTo))
                {
                    throw new CodeGenerationException("INVALID VALUE FOR VARIABLE TYPE");
                }
            }
        }
    }
}