﻿using System.Collections.Generic;
using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDK.Code_Generation.Variable_Type_Validation
{
    public static class AssignmentValidatorCreator
    {
        private static readonly Dictionary<int, IAssignmentSymbolValidator> validatorMap = new Dictionary
            <int, IAssignmentSymbolValidator>()
        {
            {TokenIdentifier.INTTYPE, new IntAssignmentValidation()},
            {TokenIdentifier.FLOATTYPE,new FloatAssignValidation() },
            {TokenIdentifier.CHARTYPE,new CharTypeValidator() }
        };
        public static IAssignmentSymbolValidator CreateAssignmentSymbolValidator(int variableType)
        {
            IAssignmentSymbolValidator symbolValidator = validatorMap[variableType];
            return symbolValidator;
        }
    }
}