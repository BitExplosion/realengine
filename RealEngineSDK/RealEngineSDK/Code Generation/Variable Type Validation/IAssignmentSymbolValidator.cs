﻿namespace RealEngineSDK.Code_Generation.Variable_Type_Validation
{
    public interface IAssignmentSymbolValidator
    {
        void validateSymbol(string arithmetic);
    }
}