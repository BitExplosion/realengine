﻿using System.Collections.Generic;
using System.Linq;
using RealEngineSDK.Code_Generation.Variable_Type_Validation;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer;

namespace RealEngineSDK.Code_Generation
{
    public class ValueAssignmentCodeGenerator : X86AssemblyCodeGenerator
    {
        private string arithmeticResult = "";
        private AssignmentOperation assignmentStatement;
        private readonly PrimitiveCollection createdVariables;

        private readonly Dictionary<int, string> variableTypeToAssemblyType = new Dictionary<int, string>
        {
            {TokenIdentifier.INTTYPE, "QWORD"},
            {TokenIdentifier.FLOATTYPE, "QWORD"},
            {TokenIdentifier.CHARTYPE, "byte"}
        };

        public ValueAssignmentCodeGenerator(PrimitiveCollection createdVariables)
        {
            this.createdVariables = createdVariables;
        }

        public List<string> generateAssemblyCode()
        {
            var variableInMemory =
                createdVariables.FirstOrDefault(
                    queryVariable => queryVariable.VariableName == assignmentStatement.Variable);
            return new List<string>
            {
                generateMovInstructionFromVariable(variableInMemory)
            };
        }

        public void assignValueToVariable(AssignmentOperation valueToAssignToVariable)
        {
            if (!createdVariables.contains(valueToAssignToVariable.Variable))
                throw new CodeGenerationException("VARIABLE DOES NOT EXIST IN PRIMITIVE COLLECTION");

            var createVariableOperation =
                createdVariables.FirstOrDefault(
                    queryVariable => queryVariable.VariableName == valueToAssignToVariable.Variable);
            var assignmentValidator =
                AssignmentValidatorCreator.CreateAssignmentSymbolValidator(createVariableOperation.VariableType);
            new AssignmentValidationRunner(createVariableOperation, valueToAssignToVariable).validateAllSymbols(
                assignmentValidator);
            assignmentStatement = valueToAssignToVariable;
        }

        public bool hasVariableBeenSet()
        {
            return assignmentStatement != null;
        }

        private string getAssemblyTypeFromVariableType(CreateVariableOperation variableInMemory)
        {
            return variableTypeToAssemblyType.Where(e => e.Key == variableInMemory.VariableType)
                .Select(e => e.Value)
                .FirstOrDefault();
        }

        private string generateMovInstructionFromVariable(CreateVariableOperation variableInMemory)
        {
            return "mov " + getAssemblyTypeFromVariableType(variableInMemory) + " " + "PTR [esp" +
                   (createdVariables.sizeOffset(variableInMemory.VariableName) != 0
                       ? "-" + createdVariables.sizeOffset(variableInMemory.VariableName)
                       : "") + "], 0x" + assignmentStatement.Arithmetic.Symbols[0];
        }


        public string solveArithmeticStatement()
        {
            if (assignmentStatement.Arithmetic.Symbols.Count == 1)
            {
                return assignmentStatement.Arithmetic.Symbols.First();
            }
            if (assignmentStatement.Arithmetic.Symbols.Count%2 == 0)
            {
                throw new CodeGenerationException("INVALID PAIRING OF ARITHMETIC OPERATION");
            }

            return arithmeticResult = new InfixCalculator.InfixCalculator(assignmentStatement).calculateInfix();
        }
    }
}