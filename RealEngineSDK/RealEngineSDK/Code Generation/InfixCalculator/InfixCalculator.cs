﻿using System.Collections.Generic;
using RealEngineSDK.Code_Generation.OperationExecuters;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDK.Code_Generation.InfixCalculator
{
    public class InfixCalculator
    {
        private readonly AssignmentOperation arithmeticToSolve;
        private readonly Stack<string> symbolStack = new Stack<string>();
        private readonly OperandUtils utils = new OperandUtils();

        public InfixCalculator(AssignmentOperation arithmeticToSolve)
        {
            this.arithmeticToSolve = arithmeticToSolve;
        }

        public string calculateInfix()
        {
            var result = "";
            foreach (var symbol in arithmeticToSolve.Arithmetic.Symbols)
            {
                solveArithmetic(symbol);
            }

            if (symbolStack.Count > 2)
            {
                throw new CodeGenerationException("INVALID NUMBER OF SYMBOLS");
            }
            return symbolStack.Pop();
        }

        private void solveArithmetic(string symbol)
        {
            if (symbol != "")
            {
                pushToStackIfOperand(symbol);
                publishAnswerToStack(symbol);
            }
        }

        private void publishAnswerToStack(string symbol)
        {
            var result = executeOnOperandPeek(symbol);
            if (result != "")
            {
                symbolStack.Push(result);
            }
        }

        private string executeOnOperandPeek(string symbol)
        {
            var result = "";
            if (utils.isTokenOperand(symbol))
            {
                result = executeOperationOnTwoSymbols(symbol, symbolStack.Pop(), symbolStack.Pop());
            }
            return result;
        }

        private void pushToStackIfOperand(string symbol)
        {
            if (!utils.isTokenOperand(symbol))
            {
                symbolStack.Push(symbol);
            }
        }


        private string executeOperationOnTwoSymbols(string operation, string rightOperand, string leftOperand)
        {
            var result = "";
            if (new OperandUtils().isTokenOperand(operation))
            {
                var operationToExecute = new OperationExecuterCreator().createExecuter(operation);
                result = operationToExecute.executeOperation(leftOperand, rightOperand);
            }


            return result;
        }
    }
}