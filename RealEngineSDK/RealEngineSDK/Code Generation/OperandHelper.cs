﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDK.Code_Generation
{
    public class OperandHelper
    {
        public bool isOperandFloat(string operand)
        {
            return (operand.Contains(".") || !operand.Contains("'"));
        }
    }
}
