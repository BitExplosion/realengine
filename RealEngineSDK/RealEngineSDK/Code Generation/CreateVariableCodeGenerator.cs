﻿using System.Collections.Generic;
using RealEngineSDK.Code_Generation;
using RealEngineSDK.SyntaxAnalyzer;

namespace RealEngineSDK.CodeGeneration
{
    public class CreateVariableCodeGenerator : X86AssemblyCodeGenerator
    {
        private readonly PrimitiveCollection createdVariables;

        public CreateVariableCodeGenerator(CreateVariableOperation variableToCreate)
            : this(new List<CreateVariableOperation> {variableToCreate})
        {
        }

        public CreateVariableCodeGenerator(List<CreateVariableOperation> variablesToCreate)
        {
            createdVariables = new PrimitiveCollection(variablesToCreate);
        }

        public CreateVariableCodeGenerator(PrimitiveCollection createdVariables)
        {
            this.createdVariables = createdVariables;
        }

        public List<string> generateAssemblyCode()
        {
            var subtractBytesFromStackAssemblyCode = new List<string>();
            foreach (var variableFromCollection in createdVariables)
            {
                subtractBytesFromStackAssemblyCode.Add("sub esp, 0x" +
                                                       createdVariables.sizeOfVariable(variableFromCollection));
            }
            return subtractBytesFromStackAssemblyCode;
        }

        public void addVariable(CreateVariableOperation variableToAdd)
        {
            createdVariables.Add(variableToAdd);
        }

        public CreateVariableOperation deleteVariable(string nameOfVariableToDelete)
        {
            return createdVariables.delete(nameOfVariableToDelete);
        }

        public int numberOfCreatedValues()
        {
            return createdVariables.Count;
        }

        public bool doesVariableExist(string name)
        {
            return createdVariables.contains(name);
        }

        public int totalSizeOfVariables()
        {
            return createdVariables.TotalSizeOfVariables();
        }

        public int positionOfVariable(string nameOfVariable)
        {
            return createdVariables.sizeOffset(nameOfVariable);
        }
    }
}