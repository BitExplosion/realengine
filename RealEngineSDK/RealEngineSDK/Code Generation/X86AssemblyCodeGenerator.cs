﻿using System.Collections.Generic;

namespace RealEngineSDK.Code_Generation
{
    internal interface X86AssemblyCodeGenerator
    {
        List<string> generateAssemblyCode();
    }
}