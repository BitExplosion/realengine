﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.Code_Generation;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDKTest.CodeGenerationTest
{
    [TestClass]
    public class ValueAssignmentGeneratorTest
    {
        [TestMethod]
        [ExpectedException(typeof(CodeGenerationException))]
        public void assignValueThatDoesNotExist()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation();
            operation.Variable="doesnotexist";
            codeGenerator.assignValueToVariable(operation);
        }

        [TestMethod]
        public void assignValueWhenVariableExistsExist()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation();
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.hasVariableBeenSet(), true);
        }

        [TestMethod]
        [ExpectedException(typeof(CodeGenerationException))]
        public void assignFloatToIntTypeValuePairing()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.3"}
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "4");
        }

        [TestMethod]
        public void evaluateAssignmentAddStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "8");
        }

        [TestMethod]
        public void evaluateAssignmentAddCharStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.CHARTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "'a'", "'b'", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "195");
        }

        [TestMethod]
        public void evaluateAssignmentFloatAddStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.5", "4.6", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "9.1");
        }

        [TestMethod]
        public void evaluateAssignmentFloatAddElevateIntStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.5", "4", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "8.5");
        }

        [TestMethod]
        public void evaluateMultipleAssignmentAddStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "+", "4", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "12");
        }

        [TestMethod]
        public void evaluateMultipleAssignmentAddFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "1.5","1.5","*" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "2.25");
        }

        [TestMethod]
        public void evaluatSingleValueAssignmentstatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "8" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "8");
        }

        [TestMethod]
        [ExpectedException(typeof(CodeGenerationException))]
        public void evaluatSingleAssignmentStatementWithoutOperation()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "8","8" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "8");
        }

        [TestMethod]
        public void evaluateAssignmentMinusStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "-" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "0");
        }

        [TestMethod]
        public void evaluateAssignmentMinusCharStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.CHARTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "'a'", "'b'", "-" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "-1");
        }

        [TestMethod]
        public void evaluateAssignmentFloatMinusStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "2.01", "-" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1.99");
        }

        [TestMethod]
        public void evaluateAssignmentMultiplyStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "*" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "16");
        }

        [TestMethod]
        public void evaluateAssignmentDivideStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "/" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }


        [TestMethod]
        public void evaluateAssignmentDivideCharStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.CHARTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "'<'", "'<'", "/" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateAssignmentFloatDivideStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "5", "2.5", "/" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "2");
        }

        [TestMethod]
        public void evaluateGreaterThanStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "5", "4", ">" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }


        [TestMethod]
        public void evaluateGreaterThanFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "5.1", "5", ">" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateGreaterThanEqualToStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", ">=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateGreaterThanEqualToFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.6", "4.5", ">=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateLesserThanStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "3", "4", "<" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateLesserThanOrEqualToStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "3", "3", "<=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateLesserThanOrEqualFloatToStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "3.1", "3.1", "<=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateLesserThanFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "3.4", "4.1", "<" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }
        [TestMethod]
        public void evaluateEqualToStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "==" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }

        [TestMethod]
        public void evaluateEqualToFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.89", "4.89", "==" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }
        [TestMethod]
        public void evaluateNotEqualToStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4", "4", "!=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "0");
        }

        [TestMethod]
        public void evaluateNotEqualToFloatStatement()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(intVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "4.4", "4", "!=" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.solveArithmeticStatement(), "1");
        }


        [TestMethod]
        [ExpectedException(typeof(CodeGenerationException))]
        public void assignCharToFloatTypeValuePairing()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation floatVariable = new CreateVariableOperation();
            floatVariable.VariableName = "variableToSearchFor";
            floatVariable.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(floatVariable);
            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            operation.Arithmetic = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation()
            {
                Symbols = new List<string>() { "'a'", "'b'", "+" }
            };
            operation.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            Assert.AreEqual(codeGenerator.hasVariableBeenSet(), true);
        }

        [TestMethod]
        public void generateAssemblyCode()
        {
            List<CreateVariableOperation> createVariables = new List<CreateVariableOperation>();
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableName = "variableToSearchFor";
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            createVariables.Add(intVariable);

            CreateVariableOperation variableFor64 = new CreateVariableOperation();
            variableFor64.VariableName = "variableFor64";
            variableFor64.VariableType = TokenIdentifier.CHARTYPE;
            createVariables.Add(variableFor64);

            CreateVariableOperation variableForFloat64 = new CreateVariableOperation();
            variableForFloat64.VariableName = "variableForFloat64";
            variableForFloat64.VariableType = TokenIdentifier.FLOATTYPE;
            createVariables.Add(variableForFloat64);

            PrimitiveCollection primtives = new PrimitiveCollection(createVariables);
            ValueAssignmentCodeGenerator codeGenerator = new ValueAssignmentCodeGenerator(primtives);
            AssignmentOperation operation = new AssignmentOperation();
            RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation arithmeticOperation = new RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic.ArithmeticOperation();
            arithmeticOperation.Symbols = new List<string>() { "6" };
            operation.Arithmetic = arithmeticOperation;
            operation.Variable = "variableToSearchFor";

            AssignmentOperation operationOffset = new AssignmentOperation();
            operationOffset.Arithmetic = arithmeticOperation;
            operationOffset.Variable = "variableToSearchFor";
            codeGenerator.assignValueToVariable(operation);
            List<string> expectedAssemblyCode = new List<string>(){
                "mov QWORD PTR [esp], 0x6"
            };
            List<string> generatedAssemblyCode = codeGenerator.generateAssemblyCode();
            CollectionAssert.AreEqual(generatedAssemblyCode,expectedAssemblyCode);

            operationOffset = new AssignmentOperation();
            operationOffset.Arithmetic = arithmeticOperation;
            operationOffset.Variable = "variableFor64";
            codeGenerator.assignValueToVariable(operationOffset);
            expectedAssemblyCode = new List<string>(){
                "mov byte PTR [esp-64], 0x6"
            };
            generatedAssemblyCode = codeGenerator.generateAssemblyCode();
            CollectionAssert.AreEqual(generatedAssemblyCode, expectedAssemblyCode);

            operationOffset = new AssignmentOperation();
            operationOffset.Arithmetic = arithmeticOperation;
            operationOffset.Variable = "variableForFloat64";
            codeGenerator.assignValueToVariable(operationOffset);
            expectedAssemblyCode = new List<string>(){
                "mov QWORD PTR [esp-65], 0x6"
            };
            generatedAssemblyCode = codeGenerator.generateAssemblyCode();
            CollectionAssert.AreEqual(generatedAssemblyCode, expectedAssemblyCode);

        }
    }
}
