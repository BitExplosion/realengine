﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.CodeGeneration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.Code_Generation;

namespace RealEngineSDKTest.CodeGenerationTest
{
    [TestClass]
    public class VariableGeneratorTest
    {
        [TestMethod]
        public void createVariableIntoStack()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTry";
            CreateVariableCodeGenerator  codeGenerator = new CreateVariableCodeGenerator(intVariable);
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 1);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTry"), true);
        }

     

        [TestMethod]
        public void createMultipleVariablesIntoCreatedVariables()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTry";
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.INTTYPE;
            charVariable.VariableName = "intVariableForTrying";
            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 2);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTry"), true);
        }


        [TestMethod]
        public void addVariableIntoStackLater()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTry";
            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.INTTYPE;
            charVariable.VariableName = "intVariableForTrying";
            codeGenerator.addVariable(charVariable);
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 2);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTrying"), true);
        }

        [TestMethod]
        [ExpectedException(typeof(CodeGenerationException))]
        public void createMultipleVariablesIntoCreatedVariablesWithSameName()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTrying";
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.INTTYPE;
            charVariable.VariableName = "intVariableForTrying";
            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 2);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTrying"), true);
        }

        [TestMethod]
        public void deleteFirstVariablesFromCreatedVariablesWithMultipleValues()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTry";
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.INTTYPE;
            charVariable.VariableName = "intVariableForTrying";
            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            CreateVariableOperation destroyedVariable = codeGenerator.deleteVariable(charVariable.VariableName);
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 1);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTry"), true);
            Assert.AreEqual(destroyedVariable.VariableName, "intVariableForTrying");
        }

        [TestMethod]
        public void deleteVariableWhenItdoesNotExistInCollection()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariableForTry";
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.INTTYPE;
            charVariable.VariableName = "intVariableForTrying";
            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            CreateVariableOperation destroyedVariable = codeGenerator.deleteVariable("somestuffthatdoesnotexist");
            Assert.AreEqual(codeGenerator.numberOfCreatedValues(), 2);
            Assert.AreEqual(codeGenerator.doesVariableExist("intVariableForTrying"), true);
            Assert.AreEqual(destroyedVariable.VariableName, "");
        }

        [TestMethod]
        public void totalSizeOfCreatedVariables()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariable";
            
            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.CHARTYPE;
            charVariable.VariableName = "charVariable";

            CreateVariableOperation floatVariable = new CreateVariableOperation();
            floatVariable.VariableType = TokenIdentifier.FLOATTYPE;
            floatVariable.VariableName = "floatVariable";

            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable,floatVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);

            Assert.AreEqual(codeGenerator.totalSizeOfVariables(), (64 * 2) + 1);
        }

        [TestMethod]
        public void PositionOfCreatedVariables()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariable";

            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.CHARTYPE;
            charVariable.VariableName = "charVariable";

            CreateVariableOperation floatVariable = new CreateVariableOperation();
            floatVariable.VariableType = TokenIdentifier.FLOATTYPE;
            floatVariable.VariableName = "floatVariable";

            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable, floatVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);

            Assert.AreEqual(codeGenerator.positionOfVariable(charVariable.VariableName), 64);
        }

        [TestMethod]
        public void generateAssembly()
        {
            CreateVariableOperation intVariable = new CreateVariableOperation();
            intVariable.VariableType = TokenIdentifier.INTTYPE;
            intVariable.VariableName = "intVariable";

            CreateVariableOperation charVariable = new CreateVariableOperation();
            charVariable.VariableType = TokenIdentifier.CHARTYPE;
            charVariable.VariableName = "charVariable";

            CreateVariableOperation floatVariable = new CreateVariableOperation();
            floatVariable.VariableType = TokenIdentifier.FLOATTYPE;
            floatVariable.VariableName = "floatVariable";

            List<CreateVariableOperation> variables = new List<CreateVariableOperation> { intVariable, charVariable, floatVariable };
            CreateVariableCodeGenerator codeGenerator = new CreateVariableCodeGenerator(variables);
            List<string> assemblyInstructions = new List<string>(){
                "sub esp, 0x64","sub esp, 0x1","sub esp, 0x64"
            };
            List<string> generatedAssemblyInstructions = codeGenerator.generateAssemblyCode();
            CollectionAssert.AreEqual(generatedAssemblyInstructions, assemblyInstructions);
        }
    }
}
