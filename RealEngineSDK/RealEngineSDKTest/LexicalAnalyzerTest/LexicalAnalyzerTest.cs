﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using System.Collections.Generic;

namespace RealEngineSDKTest.LexicalAnalyzerTest
{
    [TestClass]
    public class LexicalAnalyzerTest
    {
        private LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer();

        [TestMethod]
        public void emptySourceCode()
        {
            Assert.AreEqual(1, lexicalAnalyzer.tokenizeLine("").Count);
        }

        [TestMethod]
        public void onlyLineEndingCharacter()
        {
            Assert.AreEqual(1, lexicalAnalyzer.tokenizeLine(".").Count);
        }

        [TestMethod]
        public void singleWord()
        {
            Assert.AreEqual(1, lexicalAnalyzer.tokenizeLine("public").Count);
        }

        [TestMethod]
        public void singleWordWithEnding()
        {
            Assert.AreEqual(1, lexicalAnalyzer.tokenizeLine("public.").Count);
        }

        [TestMethod]
        public void multipleWords()
        {
            Assert.AreEqual(3, lexicalAnalyzer.tokenizeLine("public not here").Count);
        }

        [TestMethod]
        public void multipleWordsWithMultipleSpaces()
        {
            Assert.AreEqual(3, lexicalAnalyzer.tokenizeLine("public   not  here").Count);
        }

        [TestMethod]
        public void multipleWordsWithSingleSpacesAtBegining()
        {
            Assert.AreEqual(3, lexicalAnalyzer.tokenizeLine(" public   not  here").Count);
        }

        [TestMethod]
        public void multipleWordsWithMultipleSpacesAtBegining()
        {
            Assert.AreEqual(3, lexicalAnalyzer.tokenizeLine("   public   not  here").Count);
        }

        [TestMethod]
        public void multipleWordsWithMultipleNumberedSpacesAndEndingDelimiter()
        {
            Assert.AreEqual(4, lexicalAnalyzer.tokenizeLine("public not here        fam.").Count);
            Assert.AreEqual("fam", lexicalAnalyzer.tokenizeLine("public not here        fam.")[3]);
        }
        [TestMethod]
        public void tokenizeWithMultipleDelimiters()
        {
            Assert.AreEqual(8, lexicalAnalyzer.tokenizeLine("public not here        fam... try this again .. maybe.").Count);
        }

        [TestMethod]
        public void tokenizeMultipleEndingDelimiters()
        {
            Assert.AreEqual(2, lexicalAnalyzer.tokenize("public not stuff. this might be great.").Count);
        }

        [TestMethod]
        public void tokenizeAndIgnoreComments()
        {
            List<List<string>> tokens = lexicalAnalyzer.tokenize("\\comments"+Environment.NewLine+" public not stuff. public.");
            Assert.AreEqual(2, tokens.Count);
        }

        [TestMethod]
        public void tokenizeAndIgnoreCommentPlacedInTheMiddle()
        {
            List<List<string>> tokens = lexicalAnalyzer.tokenize("\\comments" + Environment.NewLine + " public not stuff."+ "\\go" + Environment.NewLine + " public.");
            Assert.AreEqual(2, tokens.Count);
        }

        [TestMethod]
        public void commentEndsWithDelimiter()
        {
            List<List<string>> tokens = lexicalAnalyzer.tokenize("\\comments." + Environment.NewLine + " public not stuff. public.");
            Assert.AreEqual(2, tokens.Count);
        }

    }
}
