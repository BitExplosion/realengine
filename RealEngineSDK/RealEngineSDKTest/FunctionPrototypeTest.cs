﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.Evaluation;
using RealEngineSDK.SyntaxAnalyzer.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class FunctionPrototypeTest
    {
        private FunctionPrototypeEvaluation evaluation = new FunctionPrototypeEvaluation();
        [TestMethod]
        public void validFunctionProtoType()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)evaluation.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName with Nothing ."));
            Assert.AreEqual(operation.OperationType, FunctionPrototypeOperation.FUNCTIONPROTOTYPEKEY);
            Assert.AreEqual(operation.ReturnType, CreateVariableOperation.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.ArgumentNames.Count, 0);
            
        }

        [TestMethod]
        public void validFunctionProtoTypeWithOneArgument()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)evaluation.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName with int variable string variableString."));
            Assert.AreEqual(operation.OperationType, FunctionPrototypeOperation.FUNCTIONPROTOTYPEKEY);
            Assert.AreEqual(operation.ReturnType, CreateVariableOperation.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.ArgumentNames.Count, 2);

        }
    }
}
