﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    public class ArithmeticTableIteratorMock : SymbolTableIterator
    {
        public bool error = false;

        public ArithmeticTableIteratorMock(ArithmeticOperation operation) : base(operation)
        {
        }

        protected override List<string> preIterateModifier(List<string> tokens)
        {
            if (error)
            {
                throw new SyntaxAnalyzerException("CANNOT PARSE THIS STATEMENT");
            }
            return new List<string>();
        }
    }
}
