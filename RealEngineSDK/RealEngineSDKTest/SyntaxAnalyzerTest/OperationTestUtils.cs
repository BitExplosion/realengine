﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.SyntaxAnalyzer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{

    [TestClass]
    public static class OperationTestUtils
    {
        
        public static void AssertTwoOperationDetailsObjects(OperationDetails actualObject, OperationDetails expectedObject)
        {
            Assert.AreEqual(actualObject.OperationType, expectedObject.OperationType);
        }
    }
}
