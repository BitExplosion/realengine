﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.LexicalAnalyzer.Utility;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.WhenStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class WhenStatementTest
    {
        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void WhenStatementWithWrongWhen()
        {
            WhenStatementSyntaxParser Parser = new WhenStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("wheny variable isGreaterThan variable2 then end."));
        }


        [TestMethod]
        public void WhenStatementExtractBooleanOperation()
        {
            WhenStatementSyntaxParser Parser = new WhenStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            WhenStatementOperation operation =  Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("when variable isGreaterThan variable2 then end.")) as WhenStatementOperation;
            Assert.AreEqual(operation.WhenCondition.Symbols[0], "variable");
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void WhenStatementNoBooleanOperation()
        {
            WhenStatementSyntaxParser Parser = new WhenStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("when then end."));
        }

        [TestMethod]
        public void WhenStatementExtractBlockCode()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableCondition";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            WhenStatementSyntaxParser Parser = new WhenStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
            WhenStatementOperation operation= Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("when variable isGreaterThan variable2 then int variableCondition  end.")) as WhenStatementOperation;
            CreateVariableOperation variable = (CreateVariableOperation) operation.BlockOperations[0];
            Assert.AreEqual(variable.VariableName, "variableCondition");
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void WhenStatementNoThen()
        {
            WhenStatementSyntaxParser Parser = new WhenStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("when end."));
        }


    }
}
