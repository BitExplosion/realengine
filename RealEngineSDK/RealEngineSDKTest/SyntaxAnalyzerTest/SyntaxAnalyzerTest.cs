﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.General;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.SyntaxAnalyzer.StatementParsers.WhenStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement;

namespace RealEngineSDKTest.LexicalAnalyzerTest
{
    [TestClass]
    public class SyntaxAnalyzerTest
    {
        [TestMethod]
        public void nonOperation()
        {
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer();
           //Operation resultantOperation = syntaxAnalyzer.syntaxAnalysisPerLine(new LexicalAnalyzer().tokenizeLine("."));
   
        }

        [TestMethod]
       
        public void variableOperation()
        {
            CreateVariableOperation operation =
                (CreateVariableOperation) new SyntaxAnalyzer().syntaxAnalysisPerLine(new LexicalAnalyzer().tokenizeLine("int variableElse"));
            Assert.AreEqual(operation.VariableName,"variableElse");
        }

        [TestMethod]

        public void ifStatementOperation()
        {
            IfStatementOperation operation =
                (IfStatementOperation)new SyntaxAnalyzer().syntaxAnalysisPerLine(new LexicalAnalyzer().tokenizeLine("if variable2 > variable then end."));
            Assert.AreEqual(operation.Condition.Symbols[0], "variable2");
        }

        [TestMethod]
        public void whenStatementOperation()
        {
            WhenStatementOperation operation =
                (WhenStatementOperation)new SyntaxAnalyzer().syntaxAnalysisPerLine(new LexicalAnalyzer().tokenizeLine("when variable > variable2 then end."));
            Assert.AreEqual(operation.WhenCondition.Symbols[0], "variable");
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void errorEvaluatingAnything()
        {
            WhenStatementOperation operation =
               (WhenStatementOperation)new SyntaxAnalyzer().syntaxAnalysisPerLine(new LexicalAnalyzer().tokenizeLine("wheny variable > variable2 then end."));
            Assert.AreEqual(operation.WhenCondition.Symbols[0], "variable");
        }





    }
}
