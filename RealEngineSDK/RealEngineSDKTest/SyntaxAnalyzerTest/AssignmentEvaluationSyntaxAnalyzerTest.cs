﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class AssignmentSyntaxParserSyntaxAnalyzerTest
    {

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void invalidAssignmentOperation()
        {
            AssignmentOperation resultantOperation = (AssignmentOperation)new AssignmentSyntaxParser(new ArithmeticTableIteratorMock(new ArithmeticOperation())).evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("variable isProbably 2 ."));
        }

        [TestMethod]
        public void validAssignmentOperationWithArithmeticOperation()
        {
            ArithmeticOperation operation = new ArithmeticOperation();
            operation.Symbols.Add("2");
            operation.Symbols.Add("4");
            operation.Symbols.Add("+");
            AssignmentOperation resultantOperation = (AssignmentOperation)new AssignmentSyntaxParser(new ArithmeticTableIteratorMock(operation)) .evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("variable assign 2 + 4 "));
            Assert.AreEqual(resultantOperation.Arithmetic.Symbols[0] ,"2");
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void validAssignmentOperationWithArithmeticOperationException()
        {
            ArithmeticOperation operation = new ArithmeticOperation();
            ArithmeticTableIteratorMock tableIterator = new ArithmeticTableIteratorMock(operation);
            tableIterator.error = true;
            operation.Symbols.Add("2");
            operation.Symbols.Add("4");
            operation.Symbols.Add("+");
            AssignmentOperation resultantOperation = (AssignmentOperation)new AssignmentSyntaxParser(tableIterator).evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("variable assign 2 + 4 "));
            Assert.AreEqual(resultantOperation.Arithmetic.LeftOperand, "2");
        }



    }
}
