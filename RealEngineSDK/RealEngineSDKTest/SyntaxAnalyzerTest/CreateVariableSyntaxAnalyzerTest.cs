﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDKTest.LexicalAnalyzerTest
{
    [TestClass]
    public class CreateVariableSyntaxAnalyzerTest
    {
        private CreateVariableSyntaxParser createVariableSyntaxParser = new CreateVariableSyntaxParser();
        [TestMethod]
        public void createIntVariable()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("int variable."));
            Assert.AreEqual(resultantOperation.VariableName, "variable");
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.INTTYPE);
        }

        [TestMethod]
        public void createIntVariableWithoutPublic()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("int variable."));
            Assert.AreEqual(resultantOperation.VariableName, "variable");
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.INTTYPE);
        }

        [TestMethod]
        public void createCharVariable()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("char variable."));
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.CHARTYPE);
        }

        [TestMethod]
        public void createFloatVariable()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("float variable."));
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.FLOATTYPE);
        }

        [TestMethod]
        public void createStringVariable()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("string variable."));
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.STRINGTYPE);
        }


        [TestMethod]
        public void createStringWithoutScopeKeywordVariable()
        {
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer();
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("string variable."));
            Assert.AreEqual(resultantOperation.VariableType, TokenIdentifier.STRINGTYPE);
        }

        [TestMethod]
        public void createStringWithScopeKeywordAndConstKeyword()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("string variable."));
            Assert.AreEqual(resultantOperation.VariableName, "variable");
        }

  
        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void invalidPrimitiveType()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("invalid variable."));
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void tooManyVariableTypeNames()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("int int variable."));
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void missingVariableName()
        {
            CreateVariableOperation resultantOperation = (CreateVariableOperation)createVariableSyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("int ."));
        }
    }
}
