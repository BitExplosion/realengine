﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser;
using RealEngineSDK.SyntaxAnalyzer.Operations;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class FunctionPrototypeTest
    {
        private FunctionPrototypeSyntaxParser SyntaxParser = new FunctionPrototypeSyntaxParser();
        [TestMethod]
        public void validFunctionProtoType()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName With Nothing end ."));
            Assert.AreEqual(operation.ReturnType, TokenIdentifier.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.Arguments.Count, 0);
            
        }

        [TestMethod]
        public void validFunctionProtoTypeWithArgument()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName With int variable end ."));
            Assert.AreEqual(operation.ReturnType, TokenIdentifier.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.Arguments.Count, 1);

        }

        [TestMethod]
        public void validFunctionProtoTypeWithMultipleArguments()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName With int variable With string variableName end ."));
            Assert.AreEqual(operation.ReturnType, TokenIdentifier.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.Arguments.Count, 2);

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void inValidFunctionProtoTypeWithoutEnd()
        {
            FunctionPrototypeOperation operation = (FunctionPrototypeOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int methodName With int variable   ."));
            Assert.AreEqual(operation.ReturnType, TokenIdentifier.INTTYPE);
            Assert.AreEqual(operation.FunctionName, "methodName");
            Assert.AreEqual(operation.Arguments.Count, 2);

        }





    }
}
