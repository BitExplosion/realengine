﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.IfStatement;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System.Collections.Generic;
using RealEngineSDK.SyntaxAnalyzer;
using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class IfStatementTest
    {
        [TestMethod]
        public void ifStatementWithoutBlockOfCode()
        {
            
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = (IfStatementOperation) Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end."));
            Assert.AreEqual(operation.Condition.Symbols[0],"variable");
            Assert.AreEqual(operation.Condition.Symbols[2],">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void ifStatementWithoutEnd()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then .")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void ifStatementWithoutInvalidEndTerminator()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then no .")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void ifStatementWithoutthen()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 end.")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void ifStatementWithInvalidThen()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 THE end.")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

        }

        [TestMethod]
        public void ifStatementWithBlockOfCode()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableCreatedByCompiler";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
            List<string> ifStatementCode = new LexicalAnalyzer().tokenizeLine("if variable > variable2 then int variable  . end.");
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then int variableCreatedByCompiler . end.")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");
            Assert.AreEqual(operation.BlockOperations.Count, 1);
            CreateVariableOperation createVariableOperation = (CreateVariableOperation)operation.BlockOperations[0];
            Assert.AreEqual(createVariableOperation.VariableName, "variableCreatedByCompiler");
            Assert.AreEqual(createVariableOperation.VariableType, TokenIdentifier.INTTYPE);
            
        }

        [TestMethod]
        public void ifStatementWithBlockOfCodeAndElse()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableCreatedByCompiler";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then int variableCreatedByCompiler . end else then end.")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");

            Assert.AreEqual(operation.BlockOperations.Count, 1);
            CreateVariableOperation createVariableOperation = (CreateVariableOperation)operation.BlockOperations[0];
            Assert.AreEqual(createVariableOperation.VariableName, "variableCreatedByCompiler");
            Assert.AreEqual(createVariableOperation.VariableType, TokenIdentifier.INTTYPE);
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[0], "");
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[2], "==");
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[1], "");


        }

        [TestMethod]
        public void ifStatementWithBlockOfCodeAndElseBlockOfCode()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableCreatedByCompilerStuff";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable isGreaterThan variable2 then int variableCreatedByCompiler . end else then int variableCreatedByCompilerStuff is 4. end.")) as IfStatementOperation;
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[0], "");
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[2], "==");
            Assert.AreEqual(operation.ElseCondition.Condition.Symbols[1], "");

            Assert.AreEqual(operation.ElseCondition.BlockOperations.Count, 1);
            CreateVariableOperation createVariableOperation2 = (CreateVariableOperation)operation.ElseCondition.BlockOperations[0];
            Assert.AreEqual(createVariableOperation2.VariableName, "variableCreatedByCompilerStuff");
            Assert.AreEqual(createVariableOperation2.VariableType, TokenIdentifier.INTTYPE);

        }

        [TestMethod]
        public void ifStatementWithBlockOfCodeAndElseBlockOfCodeWithoutElseCode()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end else then end.")) as IfStatementOperation;
            Assert.AreEqual(operation.Condition.Symbols[0], "variable");
            Assert.AreEqual(operation.Condition.Symbols[2], ">");
            Assert.AreEqual(operation.Condition.Symbols[1], "variable2");
            Assert.AreEqual(operation.BlockOperations.Count, 0);

        }

        [TestMethod]
        public void ifStatementWithoutBlockOfCodeInIf()
        {

            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end else then int variableCreatedByCompilerStuff . end.")) as IfStatementOperation;
            Assert.AreEqual(operation.BlockOperations.Count, 0);

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void elseIfWithoutCondition()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end elseif then end else then end.")) as IfStatementOperation;
            Assert.AreEqual(operation.ElseIfConditions[0].BlockOperations.Count, 1);
            Assert.AreEqual(operation.ElseIfConditions[0].BlockOperations[0], 1);
        }

        [TestMethod]
        public void emptyElseIfWithConditionStatement()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end elseif variableElse > variable2 then int variableCreatedByCompilerStuff  end else then end.")) as IfStatementOperation;
            Assert.AreEqual(operation.ElseIfConditions[0].Condition.Symbols[0], "variableElse");
        }

        [TestMethod]
        public void multipleElseIfStatements()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableElse1";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
 
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end elseif variableElse > variable2 then int variableCreatedByCompilerStuff  end elseif variable > variable2 then int variableElse1 is 5 end else then end.")) as IfStatementOperation;
            CreateVariableOperation createVariableOperation2 = (CreateVariableOperation)operation.ElseIfConditions[1].BlockOperations[0];
            Assert.AreEqual(createVariableOperation2.VariableName, "variableElse1");

        }
       
        [TestMethod]
        public void codeInElseIfStatement()
        {
            CreateVariableOperation create = new CreateVariableOperation();
            create.VariableName = "variableCreatedByCompilerStuff";
            create.VariableType = TokenIdentifier.INTTYPE;
            SyntaxAnalyzerMock syntaxAnalyzerMock = new SyntaxAnalyzerMock();
            syntaxAnalyzerMock.CreateVariableOperation = create;
            syntaxAnalyzerMock.addOperationToCollection = true;
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(syntaxAnalyzerMock, new ArithmeticSyntaxParser());
            IfStatementOperation operation = Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if variable > variable2 then end elseif variableElse > variable2 then int variableCreatedByCompilerStuff is 4 end else then end.")) as IfStatementOperation;
            CreateVariableOperation createVariableOperation2 = (CreateVariableOperation)operation.ElseIfConditions[0].BlockOperations[0];
            Assert.AreEqual(createVariableOperation2.VariableName, "variableCreatedByCompilerStuff");
            Assert.AreEqual(createVariableOperation2.VariableType, TokenIdentifier.INTTYPE);
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void ifWithoutBooleanCondition()
        {
            IfStatementSyntaxParser Parser = new IfStatementSyntaxParser(new SyntaxAnalyzerMock(), new ArithmeticSyntaxParser());
            Parser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("if then end."));

        }


    }
}
