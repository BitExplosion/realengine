﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class ArithmeticSyntaxParserTest
    {
        private ArithmeticSyntaxParser SyntaxParser = new ArithmeticSyntaxParser();
        [TestMethod]
        public void testAdditionOfTwoValues()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 4"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "4");
            Assert.AreEqual(operation.Symbols[2], "+");

        }

        [TestMethod]
        public void notBooleanOfValue()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("! 2"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "2");
            Assert.AreEqual(operation.Symbols[2], ">");

        }

        [TestMethod]
        public void testAdditionOfOneVariable()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("firstVariable + 4"));
            Assert.AreEqual(operation.Symbols[0], "firstVariable");
            Assert.AreEqual(operation.Symbols[1], "4");
            Assert.AreEqual(operation.Symbols[2], "+");
        }

        [TestMethod]
        public void testAdditionOfThreeValuesWithSamePrecedence()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 4 - 6"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "4");
            Assert.AreEqual(operation.Symbols[2], "+");
            Assert.AreEqual(operation.Symbols[3], "6");
            Assert.AreEqual(operation.Symbols[4], "-");

        }

        [TestMethod]
        public void testAdditionOfThreeValuesWithHigherPrecedence()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 4 * 6"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "4");
            Assert.AreEqual(operation.Symbols[2], "6");
            Assert.AreEqual(operation.Symbols[3], "*");
            Assert.AreEqual(operation.Symbols[4], "+");

        }

        [TestMethod]
        public void testAdditionOfFourValuesDoNotIncludeBrackets()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 4 + ( 5 + 7 )"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "4");
            Assert.AreEqual(operation.Symbols[2], "+");
            Assert.AreEqual(operation.Symbols[3], "5");
            Assert.AreEqual(operation.Symbols[4], "7");
            Assert.AreEqual(operation.Symbols[5], "+");
            Assert.AreEqual(operation.Symbols[6], "+");
        }


        [TestMethod]
        public void testAdditionOfFiveValuesVeryDifferentOrderOfPrecedence()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 3 * 4 - ( 5 * 6 ) + 1"));
            Assert.AreEqual(operation.Symbols[0], "2");
            Assert.AreEqual(operation.Symbols[1], "3");
            Assert.AreEqual(operation.Symbols[2], "4");
            Assert.AreEqual(operation.Symbols[3], "*");
            Assert.AreEqual(operation.Symbols[4], "+");
            Assert.AreEqual(operation.Symbols[5], "5");
            Assert.AreEqual(operation.Symbols[6], "6");
            Assert.AreEqual(operation.Symbols[7], "*");
            Assert.AreEqual(operation.Symbols[8], "-");
            Assert.AreEqual(operation.Symbols[9], "1");
            Assert.AreEqual(operation.Symbols[10], "+");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperandException))]
        public void testAdditionOfMoreThanTwoValues()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + \"c\""));
            
        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void testAdditionToNullType()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("+ 2"));

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void missingClosingParameter()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + 2 + ( 4 +2 "));

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperandException))]
        public void testAdditionOfKeywordType()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine(" is + 2"));

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void testAdditionToNullTypeRightOperand()
        {
            ArithmeticOperation operation = (ArithmeticOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("2 + "));
        }
       
    }
}