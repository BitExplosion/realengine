﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.LexicalAnalyzer.Utility;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class DeclarationChainedAssignmentTest
    {
        public DeclarationAssignmentChainedSyntaxParser SyntaxParser = new DeclarationAssignmentChainedSyntaxParser();

        
        public void declarationChainedWithAssignment()
        {
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer();
            DeclarationAssignmentChainedOperation resultantOperation = (DeclarationAssignmentChainedOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("public int variable assign 2 ."));
            Assert.AreEqual(resultantOperation.Assignment.Value, "2");
            Assert.AreEqual(resultantOperation.Assignment.Variable, "variable");
            Assert.AreEqual(resultantOperation.Create.VariableName, "variable");
            Assert.AreEqual(resultantOperation.Create.VariableType, TokenIdentifier.INTTYPE);
        }
    }
}
