﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEngineSDK.LexicalAnalyzer;
using RealEngineSDK.SyntaxAnalyzer.SyntaxParser.Arithmetic;
using RealEngineSDK.SyntaxAnalyzer.SyntaxAnalysisExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    [TestClass]
    public class BooleanSyntaxParserTests
    {
        private BooleanSyntaxParser SyntaxParser = new BooleanSyntaxParser();
        [TestMethod]
        public void validBooleanStatement()
        {
            BooleanOperation operation = (BooleanOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("variableName isGreaterThan variableName ."));
            Assert.AreEqual(operation.LeftOperand, "variableName");
            Assert.AreEqual(operation.RightOperand, "variableName");
            Assert.AreEqual(operation.Type, BooleanOperation.isGreaterThan);

        }

        [TestMethod]
        public void singleNegetationBooleanStatement()
        {
            BooleanOperation operation = (BooleanOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("!variableName  ."));
            Assert.AreEqual(operation.LeftOperand, "variableName");
            Assert.AreEqual(operation.Type, BooleanOperation.Negation);
            Assert.AreEqual(operation.RightOperand, "variableName");

        }

        [TestMethod]
        public void validBooleanStatementWithPrimitives()
        {
            BooleanOperation operation = (BooleanOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("true isGreaterThan false ."));
            Assert.AreEqual(operation.LeftOperand, "true");
            Assert.AreEqual(operation.RightOperand, "false");
            Assert.AreEqual(operation.Type, BooleanOperation.isGreaterThan);

        }

        [TestMethod]
        [ExpectedException(typeof(SyntaxAnalyzerException))]
        public void inValidBooleanStatement()
        {
            BooleanOperation operation = (BooleanOperation)SyntaxParser.evaluateLineOfCode(new LexicalAnalyzer().tokenizeLine("variableName isGreaterThan ."));
            Assert.AreEqual(operation.LeftOperand, "variableName");
            Assert.AreEqual(operation.RightOperand, "variableName");
            Assert.AreEqual(operation.Type, BooleanOperation.isGreaterThan);

        }
    }
}
