﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEngineSDK.SyntaxAnalyzer;

namespace RealEngineSDKTest.SyntaxAnalyzerTest
{
    class SyntaxAnalyzerMock : SyntaxAnalyzerI
    {
        private CreateVariableOperation createVariableOperation = new CreateVariableOperation();
        public Boolean addOperationToCollection = false;

        public CreateVariableOperation CreateVariableOperation
        {
            get
            {
                return createVariableOperation;
            }
            set { createVariableOperation = value; }
        }

        public List<object> analyzeSoureCode(List<string> tokens)
        {
            List<object> objects = new List<object>();
            if (addOperationToCollection)
            {
                objects.Add(createVariableOperation);
            }
            
            return objects;
        }
    }
}
